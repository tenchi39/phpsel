<?php


	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	

	header('Content-type: text/html; charset=utf-8'); 

	if(is_file('vendor/autoload.php')) {
		require __DIR__ . '/vendor/autoload.php';
	} else {
		echo '<h1>Dependencies are not installed</h1><p>Please run the following command in the command line:</p><p><i>php composer.phar install</i></p>';
		exit();
	}

	// configuration
	if(is_file('config.php')) {
		include('config.php');
	} else {
		header('Location: setup.php');
		exit();
	}


	//session
	session_name($db_database);
	session_start();
	
	// functions
	include('lib/functions.php');
	
	// language
	include('lng/'.$site_language.'.php');
	
	// permissions
	include('lib/permissions.php');
	
	// authorization
	if(!isset($_SESSION['logged_in'])) {
		header("Location: ".$baseurl."/login.php");
		exit();
	}
	
	// database
	include('lib/database.php');
	
	// delete temporary files
	purgeTmp();
	purgeExtracted();

?><!doctype html>
<html class="no-js" lang="<?php echo $site_language; ?>">
<head>
	<!-- meta -->
	<base href="<?php echo $baseurl; ?>/" />
	<meta charset="utf-8" />
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $site_title; ?></title>
	<link rel="stylesheet" href="vendor/zurb/foundation/dist/css/foundation.css">
	<link rel="stylesheet" href="css/foundation-icons.css">
	<link rel="stylesheet" href="vendor/lagman/fancybox/source/jquery.fancybox.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
	<!-- Offcanvas content -->
	<div class="off-canvas position-left" id="offCanvas" data-off-canvas>
		<ul class="vertical menu drilldown" data-drilldown>
			<li><a class="left-menu" href="index.php?view=list&amp;clear_search"><i class="fi-book"></i>&nbsp;&nbsp;<?php echo lng('my_books'); ?></a></li>
			<?php if($_SESSION['guest_session'] == false): ?>
			<li><a class="left-menu" href="index.php?view=form"><i class="fi-plus"></i>&nbsp;&nbsp;<?php echo lng('add_book'); ?></a></li>
			<li><a class="left-menu" href="index.php?view=import"><i class="fi-download"></i>&nbsp;&nbsp;<?php echo lng('import'); ?></a></li>
			<?php endif; ?>
			<li><a class="left-menu" href="index.php?view=list&amp;clear_search&amp;reading_list=1"><i class="fi-list-thumbnails"></i>&nbsp;&nbsp;<?php echo lng('on_my_reading_list'); ?></a></li>
			<li><a class="left-menu" href="index.php?view=list&amp;clear_search&amp;wishlist=1"><i class="fi-heart"></i>&nbsp;&nbsp;<?php echo lng('on_my_wishlist'); ?></a></li>
			<li><a class="left-menu" href="index.php?view=list&amp;clear_search&amp;already_read=1"><i class="fi-checkbox"></i>&nbsp;&nbsp;<?php echo lng('already_read'); ?></a></li>
			<li><a class="left-menu" href="index.php?view=list&amp;clear_search&amp;physical_copy=1"><i class="fi-book-bookmark"></i>&nbsp;&nbsp;<?php echo lng('physical_copy'); ?></a></li>
			<li><a class="left-menu" href="index.php?view=list&amp;clear_search&amp;missing=1"><i class="fi-magnifying-glass"></i>&nbsp;&nbsp;<?php echo lng('missing_books'); ?></a></li>
			<li><a class="left-menu" href="index.php?view=list&amp;clear_search&amp;missing_cover=1"><i class="fi-page-search"></i>&nbsp;&nbsp;<?php echo lng('missing_covers'); ?></a></li>
			<li><a class="left-menu" href="index.php?view=overview"><i class="fi-thumbnails"></i>&nbsp;&nbsp;<?php echo lng('collection_overview'); ?></a></li>
			<?php if($_SESSION['guest_session'] == false): ?>
			<li><a href="#"><i class="fi-wrench"></i>&nbsp;&nbsp;<?php echo lng('tools'); ?></a>
				<ul class="menu vertical nested">
					<li><a class="left-menu" href="index.php?view=tools_small_covers">&nbsp;&nbsp;<?php echo lng('tools'); ?>: <?php echo lng('small_covers'); ?></a></li>
					<li><a class="left-menu" href="index.php?view=tools_import_sequence">&nbsp;&nbsp;<?php echo lng('tools'); ?>: <?php echo lng('import_sequence'); ?></a></li>
				</ul>
			</li>
			<?php endif; ?>
			<li><a class="left-menu" href="logout.php"><i class="fi-power"></i>&nbsp;&nbsp;<?php echo lng('log_out'); ?></a></li>
		</ul>
	</div>
	<!-- Page content -->
	<div class="off-canvas-content" data-off-canvas-content>
		<div class="inner-wrap">

			<div data-sticky-container>
				<div id="header" class="top-bar" data-sticky data-options="marginTop:0;">
					<div class="top-bar-left">
						<ul class="dropdown menu" data-dropdown-menu>
							<li class="menu-text hide-for-medium cursor-pointer" data-toggle="offCanvas"><i class="fi-list"></i> <?php echo $site_title; ?></li>
							<li class="menu-text hide-for-small-only"><i class="fi-list"></i> <?php echo $site_title; ?>
								<ul class="menu vertical">
									<li><a href="index.php?view=list&amp;clear_search"><i class="fi-book"></i>&nbsp;&nbsp;<?php echo lng('my_books'); ?></a></li>
									<?php if($_SESSION['guest_session'] == false): ?>
									<li><a href="index.php?view=form"><i class="fi-plus"></i>&nbsp;&nbsp;<?php echo lng('add_book'); ?></a></li>
									<li><a href="index.php?view=import"><i class="fi-download"></i>&nbsp;&nbsp;<?php echo lng('import'); ?></a></li>
									<?php endif; ?>
									<li><a href="index.php?view=list&amp;clear_search&amp;reading_list=1"><i class="fi-list-thumbnails"></i>&nbsp;&nbsp;<?php echo lng('on_my_reading_list'); ?></a></li>
									<li><a href="index.php?view=list&amp;clear_search&amp;wishlist=1"><i class="fi-heart"></i>&nbsp;&nbsp;<?php echo lng('on_my_wishlist'); ?></a></li>
									<li><a href="index.php?view=list&amp;clear_search&amp;already_read=1"><i class="fi-checkbox"></i>&nbsp;&nbsp;<?php echo lng('already_read'); ?></a></li>
									<li><a href="index.php?view=list&amp;clear_search&amp;physical_copy=1"><i class="fi-book-bookmark"></i>&nbsp;&nbsp;<?php echo lng('physical_copy'); ?></a></li>
									<li><a href="index.php?view=list&amp;clear_search&amp;missing=1"><i class="fi-magnifying-glass"></i>&nbsp;&nbsp;<?php echo lng('missing_books'); ?></a></li>
									<li><a href="index.php?view=list&amp;clear_search&amp;missing_cover=1"><i class="fi-page-search"></i>&nbsp;&nbsp;<?php echo lng('missing_covers'); ?></a></li>
									<li><a href="index.php?view=overview"><i class="fi-thumbnails"></i>&nbsp;&nbsp;<?php echo lng('collection_overview'); ?></a></li>
									<?php if($_SESSION['guest_session'] == false): ?>
									<li><a><i class="fi-wrench"></i>&nbsp;&nbsp;<?php echo lng('tools'); ?></a>
										<ul class="menu vertical">
											<li><a href="index.php?view=tools_small_covers"><?php echo lng('small_covers'); ?></a></li>
											<li><a href="index.php?view=tools_import_sequence"><?php echo lng('import_sequence'); ?></a></li>
										</ul>
									</li>
									<?php endif; ?>
									<li><a href="logout.php"><i class="fi-power"></i>&nbsp;&nbsp;<?php echo lng('log_out'); ?></a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="top-bar-right hide-for-small-only">
						<form action="index.php?view=list" method="POST">
							<ul class="menu">
								<input type="hidden" name="clear_search">
								<li><input type="search" name="titleauthor" placeholder="<?php echo lng('search_placeholder'); ?>" value="<?php if(isset($_REQUEST['titleauthor'])){echo $_REQUEST['titleauthor'];}?>" /></li>
								<li><input type="submit" class="button" value="<?php echo lng('search');?>" /></li>
							</ul>
						</form>
					</div>
				</div>
			</div>


			<div id="main">

				<?php
					if(empty($_REQUEST['view'])) {
						$_REQUEST['view'] = 'list';
					}
					// If we are not mofifying $_SESSION values anymore, let's close the session, so multiple instances can run side by side
					if($_REQUEST['view'] != 'list') {
						session_write_close();
					}
					include('lib/'.$_REQUEST['view'].'.php');
				?>

				<?php

					// Local version
					$version_local = @file_get_contents('version.txt');
					$version_local = trim($version_local);
					// Last check
					$result = mysqli_query($link, "SELECT * FROM db WHERE db_key='version_last_check' LIMIT 1");
					$myrow = mysqli_fetch_assoc($result);
					$version_check = $myrow['db_value'];
					// If last check was not today
					if($myrow['db_value'] != date("Ymd")) {
						// Get remote version
						$version_remote = @file_get_contents('https://phpsel.randomblog.hu/version.php?key=60170e7ff20ef76e32322d9619914371&t='.base64_encode($site_title).'&v='.base64_encode($version_local));
						$version_remote = trim($version_remote);
						// Update remote version in db
						mysqli_query($link, "UPDATE db SET 
							db_value='".mysqli_real_escape_string($link, $version_remote)."'
						WHERE db_key='version_remote' LIMIT 1");
						// Update version check date in db
						mysqli_query($link, "UPDATE db SET 
							db_value='".date("Ymd")."'
						WHERE db_key='version_last_check' LIMIT 1");
					} else {
						$result = mysqli_query($link, "SELECT * FROM db WHERE db_key='version_remote' LIMIT 1");
						$myrow = mysqli_fetch_assoc($result);
						$version_remote = $myrow['db_value'];
						$version_remote = trim($version_remote);						
					}
					$version_local_num = str_replace(".", "", $version_local);
					$version_remote_num = str_replace(".", "", $version_remote);
					if($version_local < $version_remote) {
						echo '<div class="grid-container">';
						echo '<div class="grid-x grid-margin-x">';
						echo '<div class="cell small-12">';
						echo '<br />';
						echo '<br />';
						echo '<br />';
						echo '<div class="callout warning">';
						echo '<h5>'.lng('there_is_a_new_version').'</h5>';
						echo '<p>'.lng('currently_installed_version').': '.$version_local.'<br />'.lng('latest_available_version').': '.$version_remote.'</p>';
						echo '<a href="https://phpsel.randomblog.hu" target="_blank">'.lng('download_the_latest_version').'</a>';
						echo '</div>';
						echo '</div>';
						echo '</div>';
						echo '</div>';
					}
				?>
				<br />
			</div>
			<div id="footer">
				<div class="grid-container">
					<div class="grid-x grid-margin-x">
						<div class="cell small-12 text-center">
							<span class="show-for-large"><i class="fi-info"></i>&nbsp;&nbsp;</span>Version <?php echo $version_local; ?>
							<span class="hide-for-small-only">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
							<span class="show-for-small-only"><br></span>
							<a href="https://phpsel.randomblog.hu" target="_blank"><span class="show-for-large"><i class="fi-home" ></i>&nbsp;&nbsp;</span>phpsel.randomblog.hu</a>
							<span class="hide-for-small-only">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
							<span class="show-for-small-only"><br></span>
							<a href="https://phpsel.randomblog.hu/user-manual" target="_blank"><span class="show-for-large"><i class="fi-link"></i>&nbsp;&nbsp;</span>User manual</a>
							<span class="hide-for-small-only">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
							<span class="show-for-small-only"><br></span>
							<a href="https://phpsel.randomblog.hu/documentation" target="_blank"><span class="show-for-large"><i class="fi-link"></i>&nbsp;&nbsp;</span>Documentation</a>
							<span class="hide-for-small-only">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
							<span class="show-for-small-only"><br></span>
							<a href="https://www.facebook.com/PHP-Simple-Electronic-Library-688202254908770" target="_blank"><span class="show-for-large"><i class="fi-social-facebook"></i>&nbsp;&nbsp;</span>Facebook</a>
						</div>
						<div class="cell small-12 medium-3">
						</div>
						<div class="cell small-12 medium-3">
						</div>
						<div class="cell small-12 medium-3">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script src="vendor/components/jquery/jquery.min.js"></script>
	<script src="vendor/zurb/foundation/dist/js/foundation.min.js"></script>
	<script src="vendor/lagman/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="lib/functions.js"></script>

	<script>

		$(document).ready(function() {
			$(document).foundation();

			$( "#search-button" ).click(function() {
				$( "#search-container" ).slideToggle( "slow", function() {
					// Animation complete.
				});
			});
			$('#order').on('change', function () {
				var url = $(this).val(); // get selected value
				if (url) { // require a URL
					window.location = url; // redirect
				}
				return false;
			});
			<?php if($_REQUEST['view'] == 'list'): ?>
			$("body").keydown(function(e){
				// left arrow
				if ((e.keyCode || e.which) == 37) {   
				document.getElementById('previous_page_link').click();
				}
				// right arrow
				if ((e.keyCode || e.which) == 39) {
				document.getElementById('next_page_link').click();
				}   
			});
			<?php endif ?>
			// function to set the height on fly
			function autoHeight() {
				$('#main').css('min-height', 0);
				$('#main').css('min-height', (
					$(document).height() 
					- $('#header').height() 
					- $('#footer').height()
					- 35.625
				));
 			}
			// onDocumentReady function bind
			$(document).ready(function() {
				autoHeight();
			});
			// onResize bind of the function
			$(window).resize(function() {
				autoHeight();
			});
			// Initialize FancyBox
	        $('.fancybox').fancybox();

		});
	</script>

</body>
</html>