<?php
/**
 * The base configuration for phpSEL
 *
 * Rename this file to "config.php" and fll in the values.
 *
 * This file contains the following configurations:
 *
 * * Basic settings
 * * MySQL settings
 * * Login settings
 * * List page settings
 * * Details page settings
 * * Email to device settings
 *
 * @link https://phpsel.randomblog.hu
 */


// ** Basic settings ** //
/** Subdirectory - If your installation is in a subdirectory, enter the directory name with a preceeding slash, eg.: "/phpsel" */
$subdirectory = '';

/** Protocol {http/https} */
$protocol = 'http';

/** Base url - You will probably not need to change this */
$baseurl = $protocol.'://'.$_SERVER['SERVER_NAME'].$subdirectory;

/** Language - Only English is available at the moment, but you can create your own translation in the /lng directory */
$site_language = 'en';

/** Site title - Change this to your liking */
$site_title = 'phpSEL';

/** Demo mode - File upload is disabled if set to true */
$demo_mode = false;


// ** MySQL settings - You can get this info from your web host ** //
/** MySQL hostname */
$db_host = 'localhost';

/** MySQL port (optional) - For MySQL the default port is 3306, for MariaDB it is 3307 */
$db_port = '';

/** The name of the database for phpSEL */
$db_database = 'database_name_here';

/** MySQL database username */
$db_user = 'username_here';

/** MySQL database password */
$db_password = 'password_here';


// ** Login settings - Be sure to change these! ** //
/** Login username */
$login_username = 'admin';

/** Login password */
$login_password = 'adminpass';

/** Login message - You can set a message to appear under the login box */
$login_message = '';


// ** Guest login ** //
/** Guest username */
$guest_username = '';

/** Guest password */
$guest_password = '';


// ** List page settings ** //
/** Number of books on a page */
$list_items = 24;

/** Default order of books {id/title/title_original/author/genre/series/world/publisher/year_of_publication/year_of_first_publication} */
$list_default_order = 'id';

/** Default order direction of books {ASC/DESC} */
$list_default_direction = 'DESC'; // ASC or DESC

/** Maximum thumbnail width */
$list_image_max_width = 300;

/** Maximum thumbnail height */
$list_image_max_height = 300;


// ** Details page settings ** //
/** Maximum thumbnail width */
$details_image_max_width = 450;

/** Maximum thumbnail height */
$details_image_max_height = 450;

/** Show QR code to download books on your mobile {true/false} */
$qr_code = true;

/** Time format {1/2} 
 * 1 - MM/DD/YYYY HH:MM:SS
 * 2 - YYYY/MM/DD HH:MM:SS
*/
$time_format = 1;

/** Extra search links - Add your own links with author_title/title search parameters */
$search_links[] = array('https://www.google.com/search?q=', 'author_title', 'Google search');
$search_links[] = array('https://www.google.com/search?&tbm=isch&amp;q=', 'author_title', 'Google Images search');


// ** Email to device settings - Gmail settings are partly filled in ** //
/** Show Email to device button - Set to true if you have a device or app that can receive books as email attachments {true/false} */
$email_to_device = false;

/** Your device's email address - Change this! */
$email_to_device_address = 'yourdevice@yourdevice.com';

/** Your authorised email address you can send files to your device from - Change this! */
$email_to_device_from = 'youraddress@gmail.com';

/** Your SMTP server address - If you have a @gmail.com address, no need to change this */
$email_to_device_smtp_server = 'smtp.gmail.com';

/** Your SMTP username - Change this! */
$email_to_device_smtp_username = 'youraddress@gmail.com';

/** Your SMTP password - Change this! */
$email_to_device_smtp_password = 'yourpassword';

/** Your SMTP security settings - If you have a @gmail.com address, no need to change this */
$email_to_device_smtp_security = 'ssl';

/** Your SMTP port - If you have a @gmail.com address, no need to change this */
$email_to_device_smtp_port = 465;

/** File formats to show the Email to device option for - List formats your device supports */
$email_to_device_formats = array("DOC", "DOCX", "HTML", "HTM", "RTF", "JPEG", "JPG", "MOBI", "AZW", "GIF", "PNG", "BMP", "PDF"); // Amazon Kindle 5 or similar
//$email_to_device_formats = array("EPUB", "PDF", "FB2", "TXT", "DJVU", "HTM", "HTML", "DOC", "DOCX", "RTF", "CHM", "TCR", "PRC", "MOBI"); // PocketBook Touch Lux 4 or similar