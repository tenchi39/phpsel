-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2019 at 05:08 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpsel-dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `books_id` int(11) NOT NULL,
  `books_time` bigint(14) DEFAULT NULL,
  `books_author` varchar(255) NOT NULL,
  `books_title` varchar(255) NOT NULL,
  `books_title_original` varchar(255) NOT NULL,
  `books_genre` varchar(64) NOT NULL,
  `books_world` varchar(64) NOT NULL,
  `books_world_number` int(11) NOT NULL,
  `books_series` varchar(64) NOT NULL,
  `books_series_number` int(11) NOT NULL,
  `books_publisher` varchar(64) NOT NULL,
  `books_year` char(4) NOT NULL,
  `books_year_first` char(4) NOT NULL,
  `books_language` varchar(16) NOT NULL,
  `books_already_read` tinyint(1) NOT NULL,
  `books_physical_copy` tinyint(1) NOT NULL,
  `books_reading_list` tinyint(1) NOT NULL,
  `books_wishlist` tinyint(1) NOT NULL,
  `books_synopsis` text NOT NULL,
  `books_missing` tinyint(1) NOT NULL,
  `books_missing_cover` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `db`
--

CREATE TABLE `db` (
  `db_id` int(11) NOT NULL,
  `db_key` varchar(32) NOT NULL,
  `db_value` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db`
--

INSERT INTO `db` (`db_id`, `db_key`, `db_value`) VALUES
(1, 'version_remote', ''),
(2, 'version_last_check', ''),
(3, 'db_version', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`books_id`);

--
-- Indexes for table `db`
--
ALTER TABLE `db`
  ADD PRIMARY KEY (`db_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `books_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db`
--
ALTER TABLE `db`
  MODIFY `db_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
