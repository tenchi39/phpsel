<?php

echo '<div class="grid-container">';
echo '<div class="grid-x grid-padding-x">';
echo '<div class="small-12 cell">';

$permission_files = array('data/books', 'data/covers', 'data/extracted', 'data/tmp', 'data/extracted');

echo '<h3>Step 1: File permissions</h3>';
echo '<p>Checking file permissions:</p>';

echo '<p>';
$errors = false;
foreach($permission_files as $file) {
	if(is_writable($file)) {
		echo '<i class="fi-check green"></i> /'.$file.'<br />';
	} else {
		if(!file_exists($file)) {
			@mkdir($file, 0777, true);
			if(is_writable($file)) {
				echo '<i class="fi-check green"></i> /'.$file.'<br />';
			} else {
				echo '<i class="fi-x red"></i> /'.$file.' - Unable to create directory, please create manually!<br />';
				$errors = true;
			}
		} else {
			echo '<i class="fi-x red"></i> /'.$file.' - Unable to write, please change permissions!<br />';
			$errors = true;
		}
	}
}
echo '</p>';
echo '<p>These directories need to be writable by phpSEL.</p>';
echo '<br>';
echo '<a class="button secondary " href="setup.php?step=0">&laquo; '.lng('back').'</a> ';
if($errors == true) {
	echo '<a class="button" href="#" onclick="location.reload();">'.lng('try_again').'</a>';
} else {
	echo '<a class="button" href="setup.php?step=2">'.lng('next').' &raquo;</a>';
}

echo '</div>';
echo '</div>';
echo '</div>';