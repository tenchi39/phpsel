<?php
	if($_SESSION['guest_session'] == true) {
		echo unauthorizedMessage();
		return;
	}
?>
<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12">
			<br />
				<h2><?php echo lng('import_sequence'); ?></h2>
				<p><?php echo lng('import_sequence_description'); ?></p>
				<?php

					$errors = false;
					$errors_title = false;
					$errors_sequence_starting_number = false;
					$errors_sequence_ending_number = false;
					$errors_sequence_ending_higher = false;




					if(isset($_REQUEST['submitted']) and $_REQUEST['submitted'] == true) {
						if(!isset($_REQUEST['title']) or $_REQUEST['title'] == '') {
							$errors = true;
							$errors_title = true;
						}
						if(!isset($_REQUEST['sequence_starting_number']) or $_REQUEST['sequence_starting_number'] == '' or !is_numeric($_REQUEST['sequence_starting_number'])) {
							$errors = true;
							$errors_sequence_starting_number = true;
						}
						if(!isset($_REQUEST['sequence_ending_number']) or $_REQUEST['sequence_ending_number'] == '' or !is_numeric($_REQUEST['sequence_ending_number'])) {
							$errors = true;
							$errors_sequence_ending_number = true;
						}
						if(is_numeric($_REQUEST['sequence_starting_number']) and is_numeric($_REQUEST['sequence_ending_number']) and $_REQUEST['sequence_starting_number'] >= $_REQUEST['sequence_ending_number']) {
							$errors = true;
							$errors_sequence_ending_higher = true;
						}


						$_REQUEST['title'] = substr($_REQUEST['title'], 0, 250);
						$_REQUEST['title_original'] = substr($_REQUEST['title_original'], 0, 255);
						$_REQUEST['author'] = substr($_REQUEST['author'], 0, 255);
						$_REQUEST['language'] = substr($_REQUEST['language'], 0, 16);
						$_REQUEST['genre'] = substr($_REQUEST['genre'], 0, 64);
						$_REQUEST['world'] = substr($_REQUEST['world'], 0, 64);
						$_REQUEST['series'] = substr($_REQUEST['series'], 0, 64);
						$_REQUEST['publisher'] = substr($_REQUEST['publisher'], 0, 64);


						$_REQUEST['sequence_starting_number'] = substr($_REQUEST['sequence_starting_number'], 0, 5);
						$_REQUEST['sequence_ending_number'] = substr($_REQUEST['sequence_ending_number'], 0, 5);


						if($errors == false) {
							echo lng('inserted_into_the_datebase').':';
							echo '<ul>';
							for($i = $_REQUEST['sequence_starting_number']; $i <= $_REQUEST['sequence_ending_number']; $i++) {
								echo '<li>'.$_REQUEST['title']." #".$i.'</li>';
								mysqli_query($link, "INSERT INTO books (
								books_time, 
								books_title, 
								books_title_original, 
								books_author, 
								books_language, 
								books_genre, 
								books_world, 
								books_world_number, 
								books_series, 
								books_series_number, 
								books_publisher, 
								books_year, 
								books_year_first,
								books_synopsis,
								books_already_read, 
								books_physical_copy, 
								books_reading_list, 
								books_wishlist, 
								books_missing, 
								books_missing_cover
							) VALUES (
								'".date("YmdHis")."', 
								'".mysqli_real_escape_string($link, $_REQUEST['title'])." #".$i."', 
								'".mysqli_real_escape_string($link, $_REQUEST['title_original'])."', 
								'".mysqli_real_escape_string($link, $_REQUEST['author'])."', 
								'".mysqli_real_escape_string($link, $_REQUEST['language'])."', 
								'".mysqli_real_escape_string($link, $_REQUEST['genre'])."', 
								'".mysqli_real_escape_string($link, $_REQUEST['world'])."', 
								0, 
								'".mysqli_real_escape_string($link, $_REQUEST['series'])."', 
								0, 
								'".mysqli_real_escape_string($link, $_REQUEST['publisher'])."', 
								'', 
								'', 
								'', 
								'0', 
								'0', 
								'0', 
								'0', 
								'0', 
								'0'
							)");
								echo mysqli_error($link);
							}
							echo '<ul>';
						}
					}


					if(!isset($_REQUEST['title'])) {
						$_REQUEST['title'] = '';
					}
					if(!isset($_REQUEST['title_original'])) {
						$_REQUEST['title_original'] = '';
					}
					if(!isset($_REQUEST['author'])) {
						$_REQUEST['author'] = '';
					}
					if(!isset($_REQUEST['language'])) {
						$_REQUEST['language'] = '';
					}
					if(!isset($_REQUEST['genre'])) {
						$_REQUEST['genre'] = '';
					}
					if(!isset($_REQUEST['world'])) {
						$_REQUEST['world'] = '';
					}
					if(!isset($_REQUEST['series'])) {
						$_REQUEST['series'] = '';
					}
					if(!isset($_REQUEST['publisher'])) {
						$_REQUEST['publisher'] = '';
					}
					if(!isset($_REQUEST['sequence_starting_number'])) {
						$_REQUEST['sequence_starting_number'] = '';
					}
					if(!isset($_REQUEST['sequence_ending_number'])) {
						$_REQUEST['sequence_ending_number'] = '';
					}


					echo '<br />';
				
					echo '<form enctype="multipart/form-data" action="index.php?view=tools_import_sequence" method="POST">';
					echo '<input type="hidden" name="submitted" value="true" />';
					echo '<div class="grid-x">';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label';
					if($errors_title == true) {
						echo ' class="red"';
					}
					echo '>'.lng('title').' <sup>*</sup>';
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'title\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="title" name="title" type="text" value="'.$_REQUEST['title'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label>'.lng('title_original');
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'title_original\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="title_original" name="title_original" type="text" value="'.$_REQUEST['title_original'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label>'.lng('author');
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'author\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="author" name="author" type="text" value="'.$_REQUEST['author'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label>'.lng('language');
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'language\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="language" name="language" type="text" value="'.$_REQUEST['language'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label>'.lng('genre');
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'genre\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="genre" name="genre" type="text" value="'.$_REQUEST['genre'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label>'.lng('world');
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'world\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="world" name="world" type="text" value="'.$_REQUEST['world'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label>'.lng('series');
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'series\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="series" name="series" type="text" value="'.$_REQUEST['series'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label>'.lng('publisher');
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'publisher\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="publisher" name="publisher" type="text" value="'.$_REQUEST['publisher'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label';
					if($errors_sequence_starting_number == true) {
						echo ' class="red"';
					}
					echo '>'.lng('sequence_starting_number').' <sup>*</sup>';
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'sequence_starting_number\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="sequence_starting_number" name="sequence_starting_number" type="number" value="'.$_REQUEST['sequence_starting_number'].'">';
					echo '</div>';
					echo '</label>';
					echo '</div>';
					////////////////////////
					echo '<div class="small-12 cell">';
					echo '<label';
					if($errors_sequence_ending_number == true) {
						echo ' class="red"';
					}
					echo '>'.lng('sequence_ending_number').' <sup>*</sup>';
					echo '<div class="input-group">';
					echo '<span class="input-group-label">';
					echo '<a onclick="clearValue(\'sequence_ending_number\');"><i class="fi-x black"></i></a>';
					echo '</span>';
					echo '<input class="input-group-field" id="sequence_ending_number" name="sequence_ending_number" type="number" value="'.$_REQUEST['sequence_ending_number'].'">';
					echo '</div>';
					echo '</label>';
					if($errors_sequence_ending_higher == true) {
						echo '<p class="help-text red" id="sequence_ending_numberHelpText">'.lng('sequence_ending_must_be_higher').'</p>';
					}
					echo '</div>';
					////////////////////////


					echo '<div class="small-12 cell">';
					echo '<br />';
					echo '<input type="submit" class="button" value="'.lng('save').'" />';
					echo '</div>';
					echo '</div>';
					echo '</form>';
				?>

			<br>
		</div>
	</div>
</div>