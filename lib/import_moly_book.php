<?php

$i = 0;
$books = array();
foreach ($import_links as $import_link) {
	$books[$i]['link'] = 'https://moly.hu/'.$import_link;
	// Get the book page
	$html_book = curlGet($books[$i]['link']);
	// Authors
	$books[$i]['authors'] = '';
	$import_authors = strBetween($html_book, '<div class="authors">', '</div>');
	$import_authors = explode('<a href="', $import_authors);
	foreach($import_authors as $value) {
		if($books[$i]['authors'] != '') {
			$books[$i]['authors'] .= ', ';
		}
		$books[$i]['authors'] .= strBetween($value, '">', '</a>');
	}
	// Title & Series
	$books[$i]['title'] = strBetween($html_book, '<span class="item">', '</span>');
	$books[$i]['title'] = strip_tags($books[$i]['title']);
	$books[$i]['series'] = strBetween($books[$i]['title'], '(', ')');
	$books[$i]['title'] = deleteAllBetween('(', ')', $books[$i]['title']);
	$books[$i]['title'] = trim($books[$i]['title']);
	$books[$i]['series_number'] = preg_replace("/[^0-9]{1,4}/", '', $books[$i]['series']);
	$books[$i]['series'] = str_replace(' '.$books[$i]['series_number'].'.', '', $books[$i]['series']);
	// Original title
	$original_title = strBetween($html_book, '<p><strong>Eredeti mű: </strong>', '</p>');
	$original_title = explode('<span', $original_title);
	$original_title = strip_tags($original_title[0]);
	$original_title = explode(': ', $original_title);
	$books[$i]['title_original'] = '';
	$first = true;
	foreach ($original_title as $value) {
		if($first == true) {
			$first = false;
		} else {
			$books[$i]['title_original'] .= $value;
		}
	}
	// Genre
	$genre = strBetween($html_book, '<div id="tags">', '</div>');
	$genre = explode('<a ', $genre);
	$first = true;
	$books[$i]['genre'] = '';
	for($j = 1;$j<=count($genre)-1;$j++) {
		if($first == true) {
			$first = false;
		} else {
			$books[$i]['genre'] .= ', ';
		}
		$genre[$j] = '<a '.$genre[$j];
		$books[$i]['genre'] .= ucfirst(trim(strip_tags($genre[$j])));
	}
	// Synopsys
	if(inString('id="full_description"', $html_book)) {
		$books[$i]['synopsis'] = strBetween($html_book, '<div class="text" id="full_description" style="display: none"><p>', '</p></div>');
	} else {
		$books[$i]['synopsis'] = strBetween($html_book, '<div class="text"><p>', '</p></div>');
	}
	$books[$i]['synopsis'] = trim(strip_tags($books[$i]['synopsis']));
	// Covers
	$covers = '';
	$covers = strBetween($html_book, '<div class="shelf">', '<div>');
	$covers = explode('<a rel="light" ', $covers);
	$first = true;
	$books[$i]['covers'] = array();
	foreach($covers as $value) {
		if($first == true) {
			$first = false;
		} else {
			$cover = strBetween($value, 'href="', '">');
			$books[$i]['covers'][] = 'https://moly.hu/'.$cover;
		}
	}
	// Year of first publication
	$books[$i]['year_of_first_publication'] = strBetween($html_book, '<strong>Eredeti megjelenés éve: ', '</strong>');
	$books[$i]['year_of_first_publication'] = strip_tags($books[$i]['year_of_first_publication']);
	$i++;
}

?>