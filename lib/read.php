<?php

	$result = mysqli_query($link, "SELECT * FROM books WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['id'])."' LIMIT 1");
	$myrow = mysqli_fetch_assoc($result);

	// Set a higher execution time
	set_time_limit(240);

	// Delete all files from /data/extracted
	/*$files = glob('data/extracted/*'); // get all file names
	foreach($files as $file){ // iterate files
		if(is_dir($file)) {
			deleteDir($file); // delete file
		}
		if(is_file($file)) {
			unlink($file); // delete file
		}
	}*/

	// Get uploaded files
	$files = array();
	for($i = 14;$i <= 17;$i++) {
		$filename = getFilename('data/books', $myrow['books_id'].'-'.$i.'-');
		if($filename != '') {
			$files[] = $filename;
		}
	}

	$error = '';
	// Find the first cbz file
	foreach($files as $filename) {
		if(getFileExtension($filename) == 'cbz') {
			$zip = new ZipArchive;
			if ($zip->open($filename) === TRUE) {
				$zip->extractTo('data/extracted/'.$myrow['books_id']);
				$zip->close();
				$error = '';
			} else {
				$error = 'failed_to_extract_cbz_file';
			}
			break;
		} elseif(getFileExtension($filename) == 'cbr') {
			$filename = $filename;
			$filepath = 'data/extracted/'.$myrow['books_id'];

			$rar_file = rar_open($filename);
			$list = rar_list($rar_file);
			foreach($list as $file) {
			   $file->extract($filepath);
			}
			rar_close($rar_file);
		} else {
			$error = 'no_archive_found';
		}
	}

?>
<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12">
			<br />
			<?php

				//////////////
				//////////
				if($error == 'no_archive_found') {
					echo '<h2>'.lng('no_archive_found').'</h2>';
				} elseif($error == 'failed_to_extract_cbz_file') {
					echo '<h2>'.lng('failed_to_extract_cbz_file').'</h2>';
				} else {
					echo '<h2>'.$myrow['books_title'].'</h2>';
					echo '<p class="text-right"><a href="index.php?view=details&id='.$_REQUEST['id'].'">'.lng('back').' &raquo;</a></p>';
					echo '<div class="grid-x grid-margin-x">';
					$filelist = getFileList('data/extracted/'.$myrow['books_id']);
					asort($filelist);
					reset($filelist);
					$i = 1;
					foreach ($filelist as $key => $value) {
						// echo $value;
						if(!is_dir($value) and isImage($value)) {
							echo '<div class="cell small-12 medium-4 large-2 text-center">';
							echo '<a href="'.$value.'" class="fancybox" rel="gallery">';
							echo '<img src="'.$value.'" alt="" />';
							echo '</a>';
							echo '<br />';
							//$filename = explode('/', $value);
							//echo '<small>'.$filename[count($filename)-1].'</small>';
							//echo $i;
							$i++;
							//echo '<br />';
							echo '<br />';
							echo '</div>';
						}
					}
					echo '</div>';
				}


			?>
		</div>
		<div class="cell small-5">
			<?php
				// Previous
				if($myrow['books_series_number'] > 1) {
					$result2 = mysqli_query($link, "SELECT * FROM books WHERE books_series='".mysqli_real_escape_string($link, $myrow['books_series'])."' AND books_world='".mysqli_real_escape_string($link, $myrow['books_world'])."' AND books_language='".mysqli_real_escape_string($link, $myrow['books_language'])."' AND books_series_number='".($myrow['books_series_number']-1)."' LIMIT 1");
					$myrow2 = mysqli_fetch_assoc($result2);
					$show_previous = false;
					$files = array();
					for($i = 14;$i <= 17;$i++) {
						$filename = getFilename('data/books', $myrow2['books_id'].'-'.$i.'-');
						if($filename != '') {
							if(getFileExtension($filename) == 'cbz' or getFileExtension($filename) == 'cbr') {
								$show_previous = true;
							}
						}
					}
					if($show_previous == true) {
						echo '<a href="index.php?view=read&amp;id='.$myrow2['books_id'].'" data-open="readModal">&laquo; '.lng('previous_issue').'</a>';
					} else {
						echo '<a class="opacity02">&laquo; '.lng('previous_issue').'</a>';
					}
				} else {
					echo '<a class="opacity02">&laquo; '.lng('previous_issue').'</a>';
				}
			?>
		</div>
		<div class="cell small-2 text-center">
			<?php
				echo '<span id="ajax_set_read">';
				echo '<input type="checkbox" id="already_read" name="already_read"';
				if($myrow['books_already_read'] == 1) {
				 echo ' checked="checked"';
				}
				echo ' onclick="ajaxContent(\'lib/ajax_set_read.php?books_id='.$myrow['books_id'].'\', \'ajax_set_read\')">';
				echo '<label for="already_read">'.lng('already_read').'</label>';
				echo '</span>';
			?>
		</div>
		<div class="cell small-5 text-right">
			<?php
				// Previous
				if(countMysqlItems('books', "WHERE books_series='".mysqli_real_escape_string($link, $myrow['books_series'])."' AND books_world='".mysqli_real_escape_string($link, $myrow['books_world'])."' AND books_language='".mysqli_real_escape_string($link, $myrow['books_language'])."' AND books_series_number='".($myrow['books_series_number']+1)."' LIMIT 1")) {
					$result2 = mysqli_query($link, "SELECT * FROM books WHERE books_series='".mysqli_real_escape_string($link, $myrow['books_series'])."' AND books_world='".mysqli_real_escape_string($link, $myrow['books_world'])."' AND books_language='".mysqli_real_escape_string($link, $myrow['books_language'])."' AND books_series_number='".($myrow['books_series_number']+1)."' LIMIT 1");
					$myrow2 = mysqli_fetch_assoc($result2);
					$show_next = false;
					$files = array();
					for($i = 14;$i <= 17;$i++) {
						$filename = getFilename('data/books', $myrow2['books_id'].'-'.$i.'-');
						if($filename != '') {
							if(getFileExtension($filename) == 'cbz' or getFileExtension($filename) == 'cbr') {
								$show_next = true;
							}
						}
					}
					if($show_next == true) {
						echo '<a href="index.php?view=read&amp;id='.$myrow2['books_id'].'" data-open="readModal">'.lng('next_issue').' &raquo;</a>';
					} else {
						echo '<a class="opacity02">'.lng('next_issue').' &raquo;</a>';
					}
				} else {
					echo '<a class="opacity02">'.lng('next_issue').' &raquo;</a>';
				}
			?>
		</div>
	</div>
</div>

<div class="reveal" id="readModal" data-reveal>
  <h3><?php echo lng('loading_comics_reader'); ?> <img src="img/spinner.gif" alt=""></h3>
  <p class="lead"><?php echo lng('extracting_archive_to_temporary_directory'); ?></p>
  <p><?php echo lng('please_be_patient_this_might_take_a_while'); ?></p>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
