<?php

echo '<div class="grid-container">';
echo '<div class="grid-x grid-padding-x">';
echo '<div class="small-12 cell">';

echo '<h3>Welcome to phpSEL.</h3>';
echo '<p>The setup process will include the following steps:</p>';
echo '<ol>';
echo '<li>File permissions</li>';
echo '<li>Basic settings</li>';
echo '<li>Login data</li>';
echo '<li>Database connection</li>';
echo '<li>Finalization</li>';
echo '</ol>';
echo '<p>We are going to use this information to create the config.php file. <b>If for any reason automatic file creation doesn\'t work, you can create the file yourself and insert the configuration data you\'ll recieve at the end of this setup process.</b> Alternatively you can just rename config-sample.php to config.php and fill in the data yourself.</p>';
echo '<br>';
echo '<a class="button" href="setup.php?step=1">'.lng('next').' &raquo;</a>';

echo '</div>';
echo '</div>';
echo '</div>';