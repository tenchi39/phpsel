<?php

$permission_files = array('data/books', 'data/covers', 'data/tmp', 'data/extracted');

foreach($permission_files as $file) {
	if(!is_writable($file)) {
		header("Location: ".$baseurl."/permissions.php");
		exit();
	}
}