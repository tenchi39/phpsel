<?php

	if(countMysqlItems('books', "WHERE books_id=books_id") == 0) {
		echo '<br />';
		echo '<div class="grid-container">';
		echo '<div class="callout warning">';
		echo '<h5>'.lng('your_search_yielded_no_results').'</h5>';
		echo '<p>'.lng('clear_or_modify_your_search_or_add_new_items').'</p>';
		echo '</div>';
		echo '</div>';
		return;
	}

?>

<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12">
			<br />
			<?php
				echo '<h2>'.lng('collection_overview').'</h2>';
				echo '<br />';
			?>
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 medium-6 large-3">
					<?php
						echo '<h3>'.lng('languages').'</h3>';

						echo '<ul>';
						$result = mysqli_query($link, "SELECT DISTINCT books_language FROM books WHERE books_language!='' ORDER BY books_language");
						while($myrow = mysqli_fetch_assoc($result)) {
							echo  '<li><a href="index.php?clear_search&amp;language='.urlencode($myrow['books_language']).'">'.$myrow['books_language'].'</a>';
							echo  ' ('.countMysqlItems('books', "WHERE books_language='".mysqli_real_escape_string($link, $myrow['books_language'])."'").')';
							echo  '</li>';
						}
						if(countMysqlItems('books', "WHERE books_language=''") != 0) {
							echo  '<li><a href="index.php?clear_search&amp;language=N/A">'.lng('na').'</a>';
							echo  ' ('.countMysqlItems('books', "WHERE books_language=''").')';
							echo  '</li>';
						}
						echo '</ul>';

						echo '<br />';

						echo '<h3>'.lng('genres').'</h3>';

						$genres = array();
						$result = mysqli_query($link, "SELECT DISTINCT books_genre FROM books WHERE books_genre!='' ORDER BY books_genre");
						while($myrow = mysqli_fetch_assoc($result)) {
							$element = explode(',', $myrow['books_genre']);
							foreach($element as $value) {
								$value = trim($value);
								if($value != '' and $value != '-' and !in_array($value, $genres)) {
									$genres[] = $value;
								}
							}
						}
						echo '<ul>';
						if(!empty($genres)) {
							sort($genres);
							reset($genres);
							foreach($genres as $value) {
								echo '<li><a href="index.php?clear_search&amp;genre='.urlencode($value).'">'.$value.'</a>';
								echo ' ('.countMysqlItems('books', "WHERE books_genre LIKE '%".mysqli_real_escape_string($link, $value)."%'").')';
								echo '</li>';
							}
						}
						if(countMysqlItems('books', "WHERE books_genre=''") != 0) {
							echo  '<li><a href="index.php?clear_search&amp;genre=N/A">'.lng('na').'</a>';
							echo  ' ('.countMysqlItems('books', "WHERE books_genre=''").')';
							echo  '</li>';
						}
						echo '</ul>';
					?>
				</div>
				<div class="cell small-12 medium-6 large-3">
					<?php
						echo '<h3>'.lng('authors').'</h3>';

						$authors = array();
						$result = mysqli_query($link, "SELECT DISTINCT books_author FROM books WHERE books_author!='' ORDER BY books_author");
						while($myrow = mysqli_fetch_assoc($result)) {
							$element = explode(',', $myrow['books_author']);
							foreach($element as $value) {
								$value = trim($value);
								if($value != '' and !in_array($value, $authors)) {
									$authors[] = $value;
								}
							}
						}
						echo '<ul>';
						if(!empty($authors)) {
							sort($authors);
							reset($authors);
							foreach($authors as $value) {
								echo '<li><a href="index.php?clear_search&amp;author='.urlencode($value).'">'.$value.'</a>';
								echo ' ('.countMysqlItems('books', "WHERE books_author LIKE '%".mysqli_real_escape_string($link, $value)."%'").')';
								echo '</li>';
							}
						}
						if(countMysqlItems('books', "WHERE books_author=''") != 0) {
							echo  '<li><a href="index.php?clear_search&amp;author=N/A">'.lng('na').'</a>';
							echo  ' ('.countMysqlItems('books', "WHERE books_author=''").')';
							echo  '</li>';
						}
						echo '</ul>';
					?>
				</div>
				<div class="cell small-12 medium-6 large-3">
					<?php
						echo '<h3>'.lng('worlds_and_series').'</h3>';

						$result = mysqli_query($link, "SELECT DISTINCT books_world FROM books WHERE books_world!='' ORDER BY books_world");
						while($myrow = mysqli_fetch_assoc($result)) {
							echo '<b><a href="index.php?clear_search&amp;world='.urlencode($myrow['books_world']).'">'.$myrow['books_world'].'</a></b>';
							echo ' ('.countMysqlItems('books', "WHERE books_world='".mysqli_real_escape_string($link, $myrow['books_world'])."'").')';
							echo '<ul>';
							$result2 = mysqli_query($link, "SELECT DISTINCT books_series FROM books WHERE books_world='".$myrow['books_world']."' AND books_series!='' ORDER BY books_series");
							while($myrow2 = mysqli_fetch_assoc($result2)) {
								echo '<li><a href="index.php?clear_search&amp;world='.urlencode($myrow['books_world']).'&amp;series='.urlencode($myrow2['books_series']).'">'.$myrow2['books_series'].'</a>';
								echo ' ('.countMysqlItems('books', "WHERE books_world='".mysqli_real_escape_string($link, $myrow['books_world'])."' AND books_series='".mysqli_real_escape_string($link, $myrow2['books_series'])."'").')';
								echo '</li>';
							}
							echo '</ul>';
						}
						if(countMysqlItems('books', "WHERE books_world=''") != 0) {
							echo  '<b><a href="index.php?clear_search&amp;world=N/A">'.lng('na').'</a></b>';
							echo  ' ('.countMysqlItems('books', "WHERE books_world=''").')';
						}
					?>
				</div>
				<div class="cell small-12 medium-6 large-3">
					<?php
						echo '<h3>'.lng('other_series').'</h3>';

						echo '<ul>';
						$result = mysqli_query($link, "SELECT DISTINCT books_series FROM books WHERE books_world='' AND books_series!='' ORDER BY books_series");
						while($myrow = mysqli_fetch_assoc($result)) {
							echo '<li><a href="index.php?clear_search&amp;series='.urlencode($myrow['books_series']).'">'.$myrow['books_series'].'</a>';
							echo ' ('.countMysqlItems('books', "WHERE books_world='' AND books_series='".mysqli_real_escape_string($link, $myrow['books_series'])."'").')';
							echo '</li>';
						}
						if(countMysqlItems('books', "WHERE books_series=''") != 0) {
							echo  '<li><a href="index.php?clear_search&amp;series=N/A">'.lng('na').'</a>';
							echo  ' ('.countMysqlItems('books', "WHERE books_series=''").')';
							echo  '</li>';
						}
						echo '</ul>';
					?>
				</div>
			</div>

		</div>
	</div>
</div>