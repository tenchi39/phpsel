<?php

if($db_port != '') {
	$db_host .=':'.$db_port;
};

if(!@mysqli_connect($db_host, $db_user, $db_password)) {
	die('<html><head></head><body><center><br /><h1>'.lng('error').'!</h1><br />'.lng('mysql_error_1').'</center></body></html>');
	exit();
}
$link = mysqli_connect($db_host, $db_user, $db_password);
if(!@mysqli_select_db($link, $db_database)) {
	die('<html><head></head><body><center><br /><h1>'.lng('error').'!</h1><br />'.lng('mysql_error_2').'</center></body></html>');
	exit();
}
mysqli_set_charset($link, "utf8");