<?php

	if($_SESSION['guest_session'] == true) {
		echo unauthorizedMessage();
		return;
	}
	//print_r($_REQUEST);
	//print_r($_FILES);

	$errors = false;
	$errors_title = false;
	$errors_year = false;
	$errors_year_first = false;
	$errors_world_number = false;
	$errors_series_number = false;
	$errors_file = array(false, false, false, false, false);

	if(isset($_REQUEST['delete_import_cover']) and isset($_REQUEST['import_cover'])) {
		unset($_REQUEST['import_cover']);
		$errors = true;
	}

	for($i=0;$i<=4;$i++) {
		if(!isset($_REQUEST['uploaded_file_'.$i])) {
			$_REQUEST['uploaded_file_'.$i] = '';
		}
	}

	// COVER
	// If editing a book and we are deleting files
	//if(isset($_REQUEST['id']) and $_REQUEST['id'] != '') {
		if(isset($_REQUEST['delete_file_0']) and $_REQUEST['delete_file_0'] != '') {
			if(is_file('data/tmp/'.$_REQUEST['uploaded_file_0'])) {
				unlink('data/tmp/'.$_REQUEST['uploaded_file_0']);
				unset($_REQUEST['uploaded_file_0']);
				$errors = true;
			} else {
				$filename = getFilename('data/covers', $_REQUEST['id'].'-13-');
				if($filename != '') {
					unlink($filename);
					$errors = true;
				}
			}
		}
	//}

	// BOOKS
	if($demo_mode == false) {
		//if(isset($_REQUEST['id']) and $_REQUEST['id'] != '') {
			if(isset($_REQUEST['delete_file_1']) and $_REQUEST['delete_file_1'] != '') {
				if(is_file('data/tmp/'.$_REQUEST['uploaded_file_1'])) {
					unlink('data/tmp/'.$_REQUEST['uploaded_file_1']);
					unset($_REQUEST['uploaded_file_1']);
					$errors = true;
				} else {
					$filename = getFilename('data/books', $_REQUEST['id'].'-14-');
					if($filename != '') {
						unlink($filename);
						$errors = true;
					}
				}
			}
			if(isset($_REQUEST['delete_file_2']) and $_REQUEST['delete_file_2'] != '') {
				if(is_file('data/tmp/'.$_REQUEST['uploaded_file_2'])) {
					unlink('data/tmp/'.$_REQUEST['uploaded_file_2']);
					unset($_REQUEST['uploaded_file_2']);
					$errors = true;
				} else {
					$filename = getFilename('data/books', $_REQUEST['id'].'-15-');
					if($filename != '') {
						unlink($filename);
						$errors = true;
					}
				}
			}
			if(isset($_REQUEST['delete_file_3']) and $_REQUEST['delete_file_3'] != '') {
				if(is_file('data/tmp/'.$_REQUEST['uploaded_file_3'])) {
					unlink('data/tmp/'.$_REQUEST['uploaded_file_3']);
					unset($_REQUEST['uploaded_file_3']);
					$errors = true;
				} else {
					$filename = getFilename('data/books', $_REQUEST['id'].'-16-');
					if($filename != '') {
						unlink($filename);
						$errors = true;
					}
				}
			}
			if(isset($_REQUEST['delete_file_4']) and $_REQUEST['delete_file_4'] != '') {
				if(is_file('data/tmp/'.$_REQUEST['uploaded_file_4'])) {
					unlink('data/tmp/'.$_REQUEST['uploaded_file_4']);
					unset($_REQUEST['uploaded_file_4']);
					$errors = true;
				} else {
					$filename = getFilename('data/books', $_REQUEST['id'].'-17-');
					if($filename != '') {
						unlink($filename);
						$errors = true;
					}
				}
			}
		//}
	}

	$files = array();

	// COVER
	// If there are files being uploaded
	if(!empty($_FILES)) {
		$tmpdir = 'data/tmp/';
		if(!empty($_FILES['file0']) and $_FILES['file0']['name'] != '') {
			if($_FILES['file0']['error'] == 0 and isImage($_FILES['file0']['tmp_name'])) {
				$filename = '13-'.basename($_FILES['file0']['name']);
				$files[0] = $filename;
				move_uploaded_file($_FILES['file0']['tmp_name'], $tmpdir.$filename);
			} else {
				$errors = true;
				$errors_file[0] = true;
			}
		}
	}
	// BOOKS
	if($demo_mode == false) {
		// If there are files being uploaded
		if(!empty($_FILES)) {
			$tmpdir = 'data/tmp/';
			if(!empty($_FILES['file1']) and $_FILES['file1']['name'] != '') {
				if($_FILES['file1']['error'] == 0) {
					$filename = '14-'.basename($_FILES['file1']['name']);
					$files[1] = $filename;
					move_uploaded_file($_FILES['file1']['tmp_name'], $tmpdir.$filename);
				} else {
					$errors = true;
					$errors_file[1] = true;
				}
			}
			if(!empty($_FILES['file2']) and $_FILES['file2']['name'] != '') {
				if($_FILES['file2']['error'] == 0) {
					$filename = '15-'.basename($_FILES['file2']['name']);
					$files[2] = $filename;
					move_uploaded_file($_FILES['file2']['tmp_name'], $tmpdir.$filename);
				} else {
					$errors = true;
					$errors_file[2] = true;
				}
			}
			if(!empty($_FILES['file3']) and $_FILES['file3']['name'] != '') {
				if($_FILES['file3']['error'] == 0) {
					$filename = '16-'.basename($_FILES['file3']['name']);
					$files[3] = $filename;
					move_uploaded_file($_FILES['file3']['tmp_name'], $tmpdir.$filename);
				} else {
					$errors = true;
					$errors_file[3] = true;
				}
			}
			if(!empty($_FILES['file4']) and $_FILES['file4']['name'] != '') {
				if($_FILES['file4']['error'] == 0) {
					$filename = '17-'.basename($_FILES['file4']['name']);
					$files[4] = $filename;
					move_uploaded_file($_FILES['file4']['tmp_name'], $tmpdir.$filename);
				} else {
					$errors = true;
					$errors_file[4] = true;
				}
			}
		}
	}

	for($i=0;$i<=4;$i++) {
		if(isset($_REQUEST['uploaded_file_'.$i]) and $_REQUEST['uploaded_file_'.$i] != '') {
			$files[$i] = $_REQUEST['uploaded_file_'.$i];
		}
	}

	if(isset($_REQUEST['submitted']) and $_REQUEST['submitted'] == true) {
		if(!isset($_REQUEST['title']) or $_REQUEST['title'] == '') {
			$errors = true;
			$errors_title = true;
		}
		if(!isset($_REQUEST['title_original'])) {
			$_REQUEST['title_original'] = '';
		}
		if(!isset($_REQUEST['author'])) {
			$_REQUEST['author'] = '';
		}
		if(!isset($_REQUEST['language'])) {
			$_REQUEST['language'] = '';
		}
		if(!isset($_REQUEST['genre'])) {
			$_REQUEST['genre'] = '';
		}
		if(!isset($_REQUEST['world'])) {
			$_REQUEST['world'] = '';
		}
		if(!isset($_REQUEST['world_number']) or $_REQUEST['world_number'] == '') {
			$_REQUEST['world_number'] = 0;
		} else {
			if(!is_numeric($_REQUEST['world_number'])) {
				$errors = true;
				$errors_world_number = true;
			}
		}
		if(!isset($_REQUEST['series'])) {
			$_REQUEST['series'] = '';
		}
		if(!isset($_REQUEST['series_number']) or $_REQUEST['series_number'] == '') {
			$_REQUEST['series_number'] = 0;
		} else {
			if(!is_numeric($_REQUEST['series_number'])) {
				$errors = true;
				$errors_series_number = true;
			}
		}
		if(!isset($_REQUEST['publisher'])) {
			$_REQUEST['publisher'] = '';
		}
		if(!isset($_REQUEST['year_of_publication'])) {
			$_REQUEST['year_of_publication'] = '';
		}
		if(isset($_REQUEST['year_of_publication']) and $_REQUEST['year_of_publication'] != '') {
			if(!is_numeric($_REQUEST['year_of_publication']) or $_REQUEST['year_of_publication'] > date("Y")) {
				$errors = true;
				$errors_year = true;
			}
		}
		if(!isset($_REQUEST['year_of_first_publication'])) {
			$_REQUEST['year_of_first_publication'] = '';
		}
		if(isset($_REQUEST['year_of_first_publication']) and $_REQUEST['year_of_first_publication'] != '') {
			if(!is_numeric($_REQUEST['year_of_first_publication']) or $_REQUEST['year_of_first_publication'] > date("Y")) {
				$errors = true;
				$errors_year_first = true;
			}
		}
		// Shortening strings if necessary
		$_REQUEST['title'] = substr($_REQUEST['title'], 0, 255);
		$_REQUEST['title_original'] = substr($_REQUEST['title_original'], 0, 255);
		$_REQUEST['author'] = substr($_REQUEST['author'], 0, 255);
		$_REQUEST['language'] = substr($_REQUEST['language'], 0, 16);
		$_REQUEST['genre'] = substr($_REQUEST['genre'], 0, 64);
		$_REQUEST['world'] = substr($_REQUEST['world'], 0, 64);
		$_REQUEST['world_number'] = substr($_REQUEST['world_number'], 0, 11);
		$_REQUEST['series'] = substr($_REQUEST['series'], 0, 64);
		$_REQUEST['series_number'] = substr($_REQUEST['series_number'], 0, 11);
		$_REQUEST['publisher'] = substr($_REQUEST['publisher'], 0, 64);
		$_REQUEST['year_of_publication'] = substr($_REQUEST['year_of_publication'], 0, 4);
		$_REQUEST['year_of_first_publication'] = substr($_REQUEST['year_of_first_publication'], 0, 4);

		if($errors == false) {
			if(isset($_REQUEST['id'])) {
				// Update
				mysqli_query($link, "UPDATE books SET 
					books_title='".mysqli_real_escape_string($link, $_REQUEST['title'])."', 
					books_title_original='".mysqli_real_escape_string($link, $_REQUEST['title_original'])."', 
					books_author='".mysqli_real_escape_string($link, $_REQUEST['author'])."', 
					books_language='".mysqli_real_escape_string($link, $_REQUEST['language'])."', 
					books_genre='".mysqli_real_escape_string($link, $_REQUEST['genre'])."', 
					books_world='".mysqli_real_escape_string($link, $_REQUEST['world'])."', 
					books_world_number='".mysqli_real_escape_string($link, $_REQUEST['world_number'])."', 
					books_series='".mysqli_real_escape_string($link, $_REQUEST['series'])."', 
					books_series_number='".mysqli_real_escape_string($link, $_REQUEST['series_number'])."', 
					books_publisher='".mysqli_real_escape_string($link, $_REQUEST['publisher'])."', 
					books_year='".mysqli_real_escape_string($link, $_REQUEST['year_of_publication'])."', 
					books_year_first='".mysqli_real_escape_string($link, $_REQUEST['year_of_first_publication'])."', 
					books_synopsis='".mysqli_real_escape_string($link, $_REQUEST['synopsis'])."'
				WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['id'])."' lIMIT 1");
				echo mysqli_error($link);
			} else {
				// Insert
				mysqli_query($link, "INSERT INTO books (
					books_time, 
					books_title, 
					books_title_original, 
					books_author, 
					books_language, 
					books_genre, 
					books_world, 
					books_world_number, 
					books_series, 
					books_series_number, 
					books_publisher, 
					books_year, 
					books_year_first,
					books_synopsis,
					books_already_read, 
					books_physical_copy, 
					books_reading_list, 
					books_wishlist, 
					books_missing, 
					books_missing_cover
				) VALUES (
					'".date("YmdHis")."', 
					'".mysqli_real_escape_string($link, $_REQUEST['title'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['title_original'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['author'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['language'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['genre'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['world'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['world_number'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['series'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['series_number'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['publisher'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['year_of_publication'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['year_of_first_publication'])."', 
					'".mysqli_real_escape_string($link, $_REQUEST['synopsis'])."', 
					'0', 
					'0', 
					'0', 
					'0', 
					'1', 
					'1'
				)");
				echo mysqli_error($link);
				$_REQUEST['id'] = mysqli_insert_id($link);
				// If we are importing a cover image
				if(isset($_REQUEST['import_cover']) and $_REQUEST['import_cover'] != '') {
					$imported_image = curlGet($_REQUEST['import_cover']);
					$image_name = explode('/', $_REQUEST['import_cover']);
					$image_name = $image_name[count($image_name)-1];
					$image_name = explode('?', $image_name);
					$image_name = $image_name[0];
					$imported_image_file = fopen('data/covers/'.$_REQUEST['id'].'-13-'.$image_name, "w");
					fwrite($imported_image_file, $imported_image);
					fclose($imported_image_file);
				}
			}
			
			if(isset($files[0]) and $files[0]!='') {
				copy('data/tmp/'.$files[0], 'data/covers/'.$_REQUEST['id'].'-'.$files[0]);
			}
			if(isset($files[1]) and $files[1]!='') {
				copy('data/tmp/'.$files[1], 'data/books/'.$_REQUEST['id'].'-'.$files[1]);
			}
			if(isset($files[2]) and $files[2]!='') {
				copy('data/tmp/'.$files[2], 'data/books/'.$_REQUEST['id'].'-'.$files[2]);
			}
			if(isset($files[3]) and $files[3]!='') {
				copy('data/tmp/'.$files[3], 'data/books/'.$_REQUEST['id'].'-'.$files[3]);
			}
			if(isset($files[4]) and $files[4]!='') {
				copy('data/tmp/'.$files[4], 'data/books/'.$_REQUEST['id'].'-'.$files[4]);
			}
			include('lib/details.php');
			return;

		}
	}

	if(isset($_REQUEST['id']) and !isset($_REQUEST['submitted'])) {
		$result = mysqli_query($link, "SELECT * FROM books WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['id'])."' LIMIT 1");
		$myrow = mysqli_fetch_assoc($result);
		$_REQUEST['title'] = $myrow['books_title'];
		$_REQUEST['title_original'] = $myrow['books_title_original'];
		$_REQUEST['author'] = $myrow['books_author'];
		$_REQUEST['language'] = $myrow['books_language'];
		$_REQUEST['genre'] = $myrow['books_genre'];
		$_REQUEST['world'] = $myrow['books_world'];
		$_REQUEST['world_number'] = $myrow['books_world_number'];
		$_REQUEST['series'] = $myrow['books_series'];
		$_REQUEST['series_number'] = $myrow['books_series_number'];
		$_REQUEST['publisher'] = $myrow['books_publisher'];
		$_REQUEST['year_of_publication'] = $myrow['books_year'];
		$_REQUEST['year_of_first_publication'] = $myrow['books_year_first'];
		$_REQUEST['synopsis'] = $myrow['books_synopsis'];
	}
	
	if(!isset($_REQUEST['title'])) {
		$_REQUEST['title'] = '';
	}
	if(!isset($_REQUEST['title_original'])) {
		$_REQUEST['title_original'] = '';
	}
	if(!isset($_REQUEST['author'])) {
		$_REQUEST['author'] = '';
	}
	if(!isset($_REQUEST['language'])) {
		$_REQUEST['language'] = '';
	}
	if(!isset($_REQUEST['genre'])) {
		$_REQUEST['genre'] = '';
	}
	if(!isset($_REQUEST['world'])) {
		$_REQUEST['world'] = '';
	}
	
	if(!isset($_REQUEST['world_number'])) {
		$_REQUEST['world_number'] = '';
	} else {
		if($_REQUEST['world_number'] == '0') {
			$_REQUEST['world_number'] = '';
		}
	}
	if(!isset($_REQUEST['series'])) {
		$_REQUEST['series'] = '';
	}
	if(!isset($_REQUEST['series_number'])) {
		$_REQUEST['series_number'] = '';
	} else {
		if($_REQUEST['series_number'] == '0') {
			$_REQUEST['series_number'] = '';
		}
	}
	if(!isset($_REQUEST['publisher'])) {
		$_REQUEST['publisher'] = '';
	}
	if(!isset($_REQUEST['year_of_publication'])) {
		$_REQUEST['year_of_publication'] = '';
	}
	if(!isset($_REQUEST['year_of_first_publication'])) {
		$_REQUEST['year_of_first_publication'] = '';
	}
	if(!isset($_REQUEST['synopsis'])) {
		$_REQUEST['synopsis'] = '';
	}
	
?>
<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12">
			<br />
			<?php
				if(isset($_REQUEST['id'])) {
					echo '<h2>'.lng('edit_book').'</h2>';
				} else {
					echo '<h2>'.lng('add_book').'</h2>';
				}
				echo '<br />';
			
				echo '<form enctype="multipart/form-data" action="index.php?view=form" method="POST">';
				if(isset($_REQUEST['id'])) {
					echo '<input type="hidden" name="id" value="'.$_REQUEST['id'].'" />';
				}
				echo '<input type="hidden" name="submitted" value="true" />';
				echo '<div class="grid-x">';
				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label';
				if($errors_title == true) {
					echo ' class="red"';
				}
				echo '>'.lng('title').' <sup>*</sup>';
				echo '<div class="input-group">';
				echo '<span class="input-group-label">';
				echo '<a onclick="clearValue(\'title\');"><i class="fi-x black"></i></a>';
				echo '</span>';
				echo '<input class="input-group-field" id="title" name="title" type="text" value="'.$_REQUEST['title'].'">';
				echo '</div>';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label>'.lng('title_original');
				echo '<div class="input-group">';
				echo '<span class="input-group-label">';
				echo '<a onclick="clearValue(\'title_original\');"><i class="fi-x black"></i></a>';
				echo '</span>';
				echo '<input class="input-group-field" id="title_original" name="title_original" type="text" value="'.$_REQUEST['title_original'].'">';
				echo '</div>';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label>'.lng('author');
				echo '<div class="input-group">';
				echo '<span class="input-group-label">';
				echo '<a onclick="clearValue(\'author\');"><i class="fi-x black"></i></a>';
				echo '</span>';
				echo '<input class="input-group-field" id="author" name="author" type="text" value="'.$_REQUEST['author'].'">';
				echo '</div>';
				echo '<p class="help-text">';
				echo lng('you_can_add_multiple_authors');
				$value_array = array();
				$result = mysqli_query($link, "SELECT DISTINCT books_author, books_id FROM books ORDER BY books_id DESC LIMIT 10");
				echo mysqli_error($link);
				while($myrow = mysqli_fetch_assoc($result)) {
					$myrow['books_author'] = explode(',', $myrow['books_author']);
					foreach($myrow['books_author'] as $value) {
						$value = trim($value);
						if($value != '') {
							$value_array[] = trim($value);
						}
					}
				}
				$value_array = array_unique($value_array);
				if(!empty($value_array)) {
					echo '<br><small>'.lng('previous_values_in_the_database');
					$i = 0;
					$first = true;
					foreach ($value_array as $value) {
						if($first == true) {
							$first = false;
						} else {
							echo ', ';
						}
						echo '<a onclick="addValue(\'author\', \''.$value.'\');">'.$value.'</a>';
						$i++;
						if($i == 5) {
							break;
						}
					}
					echo '</small>';
				}
				echo '</p>';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label>'.lng('language');
				echo '<div class="input-group">';
				echo '<span class="input-group-label">';
				echo '<a onclick="clearValue(\'language\');"><i class="fi-x black"></i></a>';
				echo '</span>';
				echo '<input class="input-group-field" id="language" name="language" type="text" value="'.$_REQUEST['language'].'">';
				echo '</div>';
				echo '<p class="help-text">';
				$value_array = array();
				$result = mysqli_query($link, "SELECT DISTINCT books_language FROM books ORDER BY books_language");
				echo mysqli_error($link);
				while($myrow = mysqli_fetch_assoc($result)) {
					$myrow['books_language'] = explode(',', $myrow['books_language']);
					foreach($myrow['books_language'] as $value) {
						$value = trim($value);
						if($value != '') {
							$value_array[] = trim($value);
						}
					}
				}
				$value_array = array_unique($value_array);
				if(!empty($value_array)) {
					echo '<small>'.lng('previous_values_in_the_database');
					$i = 0;
					$first = true;
					foreach ($value_array as $value) {
						if($first == true) {
							$first = false;
						} else {
							echo ', ';
						}
						echo '<a onclick="clearValue(\'language\');addValue(\'language\', \''.$value.'\');">'.$value.'</a>';
						$i++;
						if($i == 5) {
							break;
						}
					}
					echo '</small>';
				}

				echo '</p>';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-11 cell">';
				echo '<label>'.lng('world');
				echo '<div class="input-group">';
				echo '<span class="input-group-label">';
				echo '<a onclick="clearValue(\'world\');"><i class="fi-x black"></i></a>';
				echo '</span>';
				echo '<input class="input-group-field" id="world" name="world" type="text" value="'.$_REQUEST['world'].'">';
				echo '</div>';
				echo '<p class="help-text">';
				$value_array = array();
				$result = mysqli_query($link, "SELECT DISTINCT books_world, books_id FROM books ORDER BY books_id DESC LIMIT 10");
				echo mysqli_error($link);
				while($myrow = mysqli_fetch_assoc($result)) {
					$myrow['books_world'] = explode(',', $myrow['books_world']);
					foreach($myrow['books_world'] as $value) {
						$value = trim($value);
						if($value != '') {
							$value_array[] = trim($value);
						}
					}
				}
				$value_array = array_unique($value_array);
				if(!empty($value_array)) {
					echo '<small>'.lng('previous_values_in_the_database');
					$i = 0;
					$first = true;
					foreach ($value_array as $value) {
						if($first == true) {
							$first = false;
						} else {
							echo ', ';
						}
						echo '<a onclick="clearValue(\'world\');addValue(\'world\', \''.$value.'\');">'.$value.'</a>';
						$i++;
						if($i == 5) {
							break;
						}
					}
					echo '</small>';
				}
				echo '</p>';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-1 cell">';
				echo '<label';
				if($errors_world_number == true) {
					echo ' class="red"';
				}
				echo '>#';
				echo '<input name="world_number" type="number" value="'.$_REQUEST['world_number'].'">';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-11 cell">';
				echo '<label>'.lng('series');
				echo '<div class="input-group">';
				echo '<span class="input-group-label">';
				echo '<a onclick="clearValue(\'series\');"><i class="fi-x black"></i></a>';
				echo '</span>';
				echo '<input class="input-group-field" id="series" name="series" type="text" value="'.$_REQUEST['series'].'">';
				echo '</div>';
				echo '<p class="help-text">';
				$value_array = array();
				$result = mysqli_query($link, "SELECT DISTINCT books_series, books_id FROM books ORDER BY books_id DESC LIMIT 10");
				echo mysqli_error($link);
				while($myrow = mysqli_fetch_assoc($result)) {
					$myrow['books_series'] = explode(',', $myrow['books_series']);
					foreach($myrow['books_series'] as $value) {
						$value = trim($value);
						if($value != '') {
							$value_array[] = trim($value);
						}
					}
				}
				$value_array = array_unique($value_array);
				if(!empty($value_array)) {
					echo '<small>'.lng('previous_values_in_the_database');
					$i = 0;
					$first = true;
					foreach ($value_array as $value) {
						if($first == true) {
							$first = false;
						} else {
							echo ', ';
						}
						echo '<a onclick="clearValue(\'series\');addValue(\'series\', \''.$value.'\');">'.$value.'</a>';
						$i++;
						if($i == 5) {
							break;
						}
					}
					echo '</small>';
				}
				echo '</p>';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-1 cell">';
				echo '<label';
				if($errors_series_number == true) {
					echo ' class="red"';
				}
				echo '>#';
				echo '<input name="series_number" type="number" value="'.$_REQUEST['series_number'].'">';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label>'.lng('genre');
				echo '<div class="input-group">';
				echo '<span class="input-group-label">';
				echo '<a onclick="clearValue(\'genre\');"><i class="fi-x black"></i></a>';
				echo '</span>';
				echo '<input class="input-group-field" id="genre" name="genre" type="text" value="'.$_REQUEST['genre'].'">';
				echo '</div>';
				echo '<p class="help-text">';
				echo lng('you_can_add_multiple_genres');
				$value_array = array();
				$result = mysqli_query($link, "SELECT DISTINCT books_genre, books_id FROM books ORDER BY books_id DESC LIMIT 10");
				echo mysqli_error($link);
				while($myrow = mysqli_fetch_assoc($result)) {
					$myrow['books_genre'] = explode(',', $myrow['books_genre']);
					foreach($myrow['books_genre'] as $value) {
						$value = trim($value);
						if($value != '') {
							$value_array[] = trim($value);
						}
					}
				}
				$value_array = array_unique($value_array);
				if(!empty($value_array)) {
					echo '<br><small>'.lng('previous_values_in_the_database');
					$i = 0;
					$first = true;
					foreach ($value_array as $value) {
						if($first == true) {
							$first = false;
						} else {
							echo ', ';
						}
						echo '<a onclick="addValue(\'genre\', \''.$value.'\');">'.$value.'</a>';
						$i++;
						if($i == 5) {
							break;
						}
					}
					echo '</small>';
				}
				echo '</p>';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label>'.lng('publisher');
				echo '<div class="input-group">';
				echo '<span class="input-group-label">';
				echo '<a onclick="clearValue(\'publisher\');"><i class="fi-x black"></i></a>';
				echo '</span>';
				echo '<input class="input-group-field" id="publisher" name="publisher" type="text" value="'.$_REQUEST['publisher'].'">';
				echo '</div>';
				echo '<p class="help-text">';
				$value_array = array();
				$result = mysqli_query($link, "SELECT DISTINCT books_publisher, books_id FROM books ORDER BY books_id DESC LIMIT 10");
				echo mysqli_error($link);
				while($myrow = mysqli_fetch_assoc($result)) {
					$myrow['books_publisher'] = explode(',', $myrow['books_publisher']);
					foreach($myrow['books_publisher'] as $value) {
						$value = trim($value);
						if($value != '') {
							$value_array[] = trim($value);
						}
					}
				}
				$value_array = array_unique($value_array);
				if(!empty($value_array)) {
					echo '<small>'.lng('previous_values_in_the_database');
					$i = 0;
					$first = true;
					foreach ($value_array as $value) {
						if($first == true) {
							$first = false;
						} else {
							echo ', ';
						}
						echo '<a onclick="clearValue(\'publisher\');addValue(\'publisher\', \''.$value.'\');">'.$value.'</a>';
						$i++;
						if($i == 5) {
							break;
						}
					}
					echo '</small>';
				}
				echo '</p>';
				echo '</label>';
				echo '</div>';



				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label';
				if($errors_year == true) {
					echo ' class="red"';
				}
				echo '>'.lng('year_of_publication');
				echo '<input name="year_of_publication" type="number" value="'.$_REQUEST['year_of_publication'].'">';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label';
				if($errors_year_first == true) {
					echo ' class="red"';
				}
				echo '>'.lng('year_of_first_publication');
				echo '<input name="year_of_first_publication" type="number" value="'.$_REQUEST['year_of_first_publication'].'">';
				echo '</label>';
				echo '</div>';
				////////////////////////
				echo '<div class="small-12 cell">';
				echo '<label>'.lng('synopsis');
				echo '<textarea name="synopsis" class="height250px">'.$_REQUEST['synopsis'].'</textarea>';
				echo '<input type="submit" class="button hidden" value="'.lng('submit').'" />';
				echo '</label>';
				echo '</div>';
				////////////////////////
				$max_file_size = humanReadableSize(file_upload_max_size());
				////////////////////////
				if(isset($_REQUEST['import_cover']) and $_REQUEST['import_cover'] != '') {

					echo '<input type="hidden" name="import_cover" value="'.$_REQUEST['import_cover'].'" />';
					echo '<div class="small-12 cell">';
						echo '<label>'.lng('upload_cover').'</label>';
						echo '<div class="grid-x">';
							echo '<div class="small-12 medium-10 cell">';
								echo '<input type="text" value="'.$_REQUEST['import_cover'].'" disabled="disabled" />';
							echo '</div>';
							echo '<div class="small-12 medium-2 cell">';
								echo '<input type="submit" name="delete_import_cover" class="button button-margin-bottom-1rem alert expanded" value="'.lng('delete').'" onclick="return confirm(\''.lng('are_you_sure').'\')" />';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				} else {
					////////////////////////
					unset($filename);
					$hide_file_upload = false;
					if(isset($_REQUEST['id']) and $_REQUEST['id'] != '') {
						$filename = getFilename('data/covers', $_REQUEST['id'].'-13-');
						if($filename != '') {
							$hide_file_upload = true;
						}
					}
					if($hide_file_upload == false) {
						echo '<div class="small-12 cell">';
						echo '<label';
						if($errors_file[0] == true) {
							echo ' class="red"';
						}
						echo '>'.lng('upload_cover').'</label>';
						echo '<div class="grid-x">';
						if(!empty($files[0])) {
							echo '<div class="small-12 medium-10 cell">';
							echo '<input name="uploaded_file_0" type="text" value="'.$files[0].'" readonly="readonly" />';
							echo '</div>';
							echo '<div class="small-12 medium-2 cell">';
							echo '<input type="submit" name="delete_file_0" class="button button-margin-bottom-1rem alert expanded" value="'.lng('delete').'" onclick="return confirm(\'Are you sure?\')" />';
							echo '</div>';
						} else {
							echo '<div class="small-12 medium-10 cell">';
							echo '<input id="file0fakepath" type="text" readonly="readonly" />';
							echo '</div>';
							echo '<div class="small-12 medium-2 cell">';
							echo '<label for="file0" class="button expanded button-margin-bottom-1rem">'.lng('choose_file').'</label>';
							echo '<input type="file" id="file0" name="file0" onchange="document.getElementById(\'file0fakepath\').value=document.getElementById(\'file0\').value;" class="show-for-sr">';
							echo '</div>';
						}
						echo '<p class="help-text">'.lng('maximum_file_size').$max_file_size.'</p>';
						echo '</div>';
						echo '</div>';
					} else {
						echo '<div class="small-12 cell">';
						echo '<label>'.lng('upload_cover').'</label>';
						echo '<div class="grid-x">';
						echo '<div class="small-12 medium-10 cell">';
						echo '<input name="already_uploaded_file_0" type="text" value="'.$filename.'" readonly="readonly" />';
						echo '</div>';
						echo '<div class="small-12 medium-2 cell">';
						echo '<input type="submit" name="delete_file_0" class="button button-margin-bottom-1rem alert expanded" value="'.lng('delete').'" onclick="return confirm(\''.lng('are_you_sure').'\')" />';
						echo '</div>';
						echo '<p class="help-text">'.lng('maximum_file_size').$max_file_size.'</p>';
						echo '</div>';
						echo '</div>';
					}
				}
				////////////////////////
				for($i=1;$i<=4;$i++) {
					unset($filename);
					$hide_file_upload = false;
					if(isset($_REQUEST['id']) and $_REQUEST['id'] != '') {
						$filename = getFilename('data/books', $_REQUEST['id'].'-'.(13+$i).'-');
						if($filename != '') {
							$hide_file_upload = true;
						}
					}
					if($hide_file_upload == false) {

						echo '<div class="small-12 cell">';
						echo '<label';
						if($errors_file[$i] == true) {
							echo ' class="red"';
						}
						echo '>'.lng('upload_book').$i.'</label>';
						echo '<div class="grid-x">';
						if($demo_mode == false) {
							if(!empty($files[$i])) {
								echo '<div class="small-12 medium-10 cell">';
								echo '<input name="uploaded_file_'.$i.'" type="text" value="'.$files[$i].'" readonly="readonly" />';
								echo '</div>';
								echo '<div class="small-12 medium-2 cell">';
								echo '<input type="submit" name="delete_file_'.$i.'" class="button button-margin-bottom-1rem alert expanded" value="'.lng('delete').'" onclick="return confirm(\'Are you sure?\')" />';
								echo '</div>';
							} else {
								echo '<div class="small-12 medium-10 cell">';
								echo '<input id="file'.$i.'fakepath" type="text" readonly="readonly" />';
								echo '</div>';
								echo '<div class="small-12 medium-2 cell">';
								echo '<label for="file'.$i.'" class="button expanded button-margin-bottom-1rem">'.lng('choose_file').'</label>';
								echo '<input type="file" id="file'.$i.'" name="file'.$i.'" onchange="document.getElementById(\'file'.$i.'fakepath\').value=document.getElementById(\'file'.$i.'\').value;" class="show-for-sr">';
								echo '</div>';
							}
							echo '<p class="help-text">'.lng('maximum_file_size').$max_file_size.'</p>';
						} else {
							echo '<input type="text" value="'.lng('file_upload_is_disabled_in_demo_mode').'" disabled="disabled" />';
						}
						echo '</div>';
						echo '</div>';
					} else {
						echo '<div class="small-12 cell">';
						echo '<label>'.lng('upload_book').$i.'</label>';
						echo '<div class="grid-x">';
						if($demo_mode == false) {
							echo '<div class="small-12 medium-10 cell">';
							echo '<input name="already_uploaded_file_'.$i.'" type="text" value="'.$filename.'" readonly="readonly" />';
							echo '</div>';
							echo '<div class="small-12 medium-2 cell">';
							echo '<input type="submit" name="delete_file_'.$i.'" class="button button-margin-bottom-1rem alert expanded" value="'.lng('delete').'" onclick="return confirm(\''.lng('are_you_sure').'\')" />';
							echo '</div>';
						} else {
							echo '<div class="small-12 cell">';
							echo '<input type="text" value="'.lng('file_deletion_is_disabled_in_demo_mode').'" disabled="disabled" />';
							echo '</div>';
						}
						echo '<p class="help-text">'.lng('maximum_file_size').$max_file_size.'</p>';
						echo '</div>';
						echo '</div>';
					}
				}

				echo '<div class="small-12 cell">';
				echo '<br />';
				echo '<input type="submit" class="button" value="'.lng('save').'" />';
				echo '</div>';
				echo '</div>';
				echo '</form>';

			?>
			<br>
			<script type="text/javascript">
				document.getElementById("title").focus();
			</script>
		</div>
	</div>
</div>