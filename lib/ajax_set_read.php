<?php

session_start();

if($_SESSION['guest_session'] == true) {
	exit;
}

// configuration
include('../config.php');
// functions
include('../lib/functions.php');
// database
include('../lib/database.php');
// language
include('../lng/'.$site_language.'.php');

$result = mysqli_query($link, "SELECT * FROM books WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
$myrow = mysqli_fetch_assoc($result);

if($myrow['books_already_read'] == 1) {
	echo '<input type="checkbox" id="already_read" name="already_read" onclick="ajaxContent(\'lib/ajax_set_read.php?books_id='.$myrow['books_id'].'\', \'ajax_set_read\')">';
	echo '<label for="already_read">'.lng('already_read').'</label>';
	mysqli_query($link, "UPDATE books SET books_already_read=0 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
} else {
	echo '<input type="checkbox" id="already_read" name="already_read" onclick="ajaxContent(\'lib/ajax_set_read.php?books_id='.$myrow['books_id'].'\', \'ajax_set_read\')" checked="checked">';
	echo '<label for="already_read">'.lng('already_read').'</label>';
	mysqli_query($link, "UPDATE books SET books_already_read=1 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
}