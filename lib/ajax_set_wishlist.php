<?php

session_start();

if($_SESSION['guest_session'] == true) {
	exit;
}

// configuration
include('../config.php');
// functions
include('../lib/functions.php');
// database
include('../lib/database.php');
// language
include('../lng/'.$site_language.'.php');

$result = mysqli_query($link, "SELECT * FROM books WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
$myrow = mysqli_fetch_assoc($result);

if($myrow['books_wishlist'] == 1) {
	echo '<input type="checkbox" id="wishlist" name="wishlist" onclick="ajaxContent(\'lib/ajax_set_wishlist.php?books_id='.$myrow['books_id'].'\', \'ajax_set_wishlist\')">';
	echo '<label for="wishlist">'.lng('wishlist').'</label>';
	mysqli_query($link, "UPDATE books SET books_wishlist=0 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
} else {
	echo '<input type="checkbox" id="wishlist" name="wishlist" onclick="ajaxContent(\'lib/ajax_set_wishlist.php?books_id='.$myrow['books_id'].'\', \'ajax_set_wishlist\')" checked="checked">';
	echo '<label for="wishlist">'.lng('wishlist').'</label>';
	mysqli_query($link, "UPDATE books SET books_wishlist=1 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
}