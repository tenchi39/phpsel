<?php

session_start();

if($_SESSION['guest_session'] == true) {
	exit;
}

// configuration
include('../config.php');
// functions
include('../lib/functions.php');
// database
include('../lib/database.php');
// language
include('../lng/'.$site_language.'.php');

$result = mysqli_query($link, "SELECT * FROM books WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
$myrow = mysqli_fetch_assoc($result);

if($myrow['books_physical_copy'] == 1) {
	echo '<input type="checkbox" id="physical_copy" name="physical_copy" onclick="ajaxContent(\'lib/ajax_set_physical_copy.php?books_id='.$myrow['books_id'].'\', \'ajax_set_physical_copy\')">';
	echo '<label for="physical_copy">'.lng('physical_copy').'</label>';
	mysqli_query($link, "UPDATE books SET books_physical_copy=0 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
} else {
	echo '<input type="checkbox" id="physical_copy" name="physical_copy" onclick="ajaxContent(\'lib/ajax_set_physical_copy.php?books_id='.$myrow['books_id'].'\', \'ajax_set_physical_copy\')" checked="checked">';
	echo '<label for="physical_copy">'.lng('physical_copy').'</label>';
	mysqli_query($link, "UPDATE books SET books_physical_copy=1 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['books_id'])."' LIMIT 1");
}
