<?php

if(isset($_REQUEST['setup_username'])) {
	$_SESSION['setup_username'] = $_REQUEST['setup_username'];
}
if(isset($_REQUEST['setup_password'])) {
	$_SESSION['setup_password'] = $_REQUEST['setup_password'];
}

$errors = false;
////////////////
if(!isset($_SESSION['setup_library_name']) or $_SESSION['setup_library_name'] == '') {
	$errors = true;
}
////////////////
if(!isset($_SESSION['setup_username']) or $_SESSION['setup_username'] == '') {
	$errors = true;
}
if(!isset($_SESSION['setup_password']) or $_SESSION['setup_password'] == '') {
	$errors = true;
}
////////////////
if($errors == true) {
	header('Location: setup.php?step=3&errors=true');
	exit();
}


echo '<form action="setup.php?step=5" method="POST">';
echo '<div class="grid-container">';
echo '<div class="grid-x grid-padding-x">';

echo '<div class="small-12 cell">';
echo '<h3>Step 4: Database connection</h3>';
echo '<p>Please enter your database connection details. If you are unsure about these, contact your host.</p>';
echo '</div>';

echo '<div class="small-12 cell">';
echo '<label';
if(isset($_REQUEST['errors']) and $_REQUEST['errors'] == true and (!isset($_SESSION['setup_db_host']) or $_SESSION['setup_db_host'] == '')) {
	echo ' class="red"';
};
echo '>Database host';
echo '<input type="text" name="setup_db_host" value="';
if(isset($_SESSION['setup_db_host'])) {
	echo $_SESSION['setup_db_host'];
} else {
	echo 'localhost';
}
echo '">';
echo '</label>';
echo '<p class="help-text">Enter the host name of the database. The default value for XAMPP is \'localhost\'.</p>';
echo '</div>';

echo '<div class="small-12 cell">';
echo '<label>Database port <small>(optional)</small>';
echo '<input type="text" name="setup_db_port" value="';
if(isset($_SESSION['setup_db_port'])) {
	echo $_SESSION['setup_db_port'];
} else {
	echo '3306';
}
echo '">';
echo '</label>';
echo '<p class="help-text">The default port for MySQL is 3306. The default port for MariaDB is 3307.</p>';
echo '</div>';

echo '<div class="small-12 cell">';
echo '<label';
if(isset($_REQUEST['errors']) and $_REQUEST['errors'] == true and (!isset($_SESSION['setup_db_name']) or $_SESSION['setup_db_name'] == '')) {
	echo ' class="red"';
};
echo '>Database name';
echo '<input type="text" name="setup_db_name" value="';
if(isset($_SESSION['setup_db_name'])) {
	echo $_SESSION['setup_db_name'];
} else {
	echo 'phpsel';
}
echo '">';
echo '</label>';
echo '<p class="help-text">You can change the database name to anything you like. If you plan to run several phpSEL installations on the same machine, use a different database name for each of them.</p>';
echo '</div>';

echo '<div class="small-12 cell">';
echo '<label';
if(isset($_REQUEST['errors']) and $_REQUEST['errors'] == true and (!isset($_SESSION['setup_db_user']) or $_SESSION['setup_db_user'] == '')) {
	echo ' class="red"';
};
echo '>Database username';
echo '<input type="text" name="setup_db_user" value="';
if(isset($_SESSION['setup_db_user'])) {
	echo $_SESSION['setup_db_user'];
} else {
	echo 'root';
}
echo '">';
echo '</label>';
echo '<p class="help-text">Enter the user you can access the database with. The default user for XAMPP is \'root\'.</p>';
echo '</div>';

echo '<div class="small-12 cell">';
echo '<label';
if(isset($_REQUEST['errors']) and $_REQUEST['errors'] == true and (!isset($_SESSION['setup_db_password']) or $_SESSION['setup_db_password'] == '')) {
	echo ' class="red"';
};
echo '>Database password';
echo '<input type="password" name="setup_db_password" value="';
if(isset($_SESSION['setup_db_password'])) {
	echo $_SESSION['setup_db_password'];
} else {
	echo '';
}
echo '">';
echo '</label>';
echo '<p class="help-text">Enter the password for the database user. The default password for root in XAMPP is empty, so leave this field empty too.</p>';
echo '</div>';


echo '<div class="medium-12 cell">';
echo '<br />';
echo '<a class="button secondary" href="setup.php?step=3">&laquo; '.lng('back').'</a> ';
echo '<input type="submit" class="button" value="'.lng('next').' &raquo;">';
echo '</div>';

echo '</div>';
echo '</div>';
echo '</form>';