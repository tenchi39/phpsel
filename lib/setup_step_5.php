<?php

if(isset($_REQUEST['setup_db_host'])) {
	$_SESSION['setup_db_host'] = $_REQUEST['setup_db_host'];
}
if(isset($_REQUEST['setup_db_port'])) {
	$_SESSION['setup_db_port'] = $_REQUEST['setup_db_port'];
}
if(isset($_REQUEST['setup_db_name'])) {
	$_SESSION['setup_db_name'] = $_REQUEST['setup_db_name'];
}
if(isset($_REQUEST['setup_db_user'])) {
	$_SESSION['setup_db_user'] = $_REQUEST['setup_db_user'];
}
if(isset($_REQUEST['setup_db_password'])) {
	$_SESSION['setup_db_password'] = $_REQUEST['setup_db_password'];
}

$errors = false;
if(!isset($_SESSION['setup_db_host']) or $_SESSION['setup_db_host'] == '') {
	$errors = true;
}
if(!isset($_SESSION['setup_db_name']) or $_SESSION['setup_db_name'] == '') {
	$errors = true;
}
if(!isset($_SESSION['setup_db_user']) or $_SESSION['setup_db_user'] == '') {
	$errors = true;
}
if(!isset($_SESSION['setup_db_password'])) {
	$errors = true;
}
if($errors == true) {
	header('Location: setup.php?step=4&errors=true');
	exit();
}

$errors_database = false;

echo '<div class="grid-container">';
echo '<div class="grid-x grid-padding-x">';

echo '<div class="small-12 cell">';
echo '<h3>Step 5: Finalization</h3>';
// 1) Installing the database

$db_host = $_SESSION['setup_db_host'];
$db_port = $_SESSION['setup_db_port'];
$db_database = $_SESSION['setup_db_name'];
$db_user = $_SESSION['setup_db_user'];
$db_password = $_SESSION['setup_db_password'];

echo '<p>';

if($db_port != '') {
	$db_host .=':'.$db_port;
};
// Checking database connection
if(!@mysqli_connect($db_host, $db_user, $db_password)) {
	echo '<i class="fi-x red"></i> Could not connect to the database! Please go <a href="setup.php?step=4">back</a> and check the connection parameters.<br />';
	echo '<br />';
	echo '<a class="button secondary" href="setup.php?step=4">&laquo; '.lng('back').'</a> ';
	$errors_database = true;
} else {
	// If successful, checking database and table connection
	$link = mysqli_connect($db_host, $db_user, $db_password);
	if(!@mysqli_select_db($link, $db_database)) {
		echo '<i class="fi-x red"></i> Connection is successful, but could not access the database specified! Please go <a href="setup.php?step=4">back</a> and check the <b>database name</b>.<br />';
		echo '<br />';
		echo '<a class="button secondary" href="setup.php?step=4">&laquo; '.lng('back').'</a> ';
		$errors_database = true;
	} else {
		// If successful, checking if there is already a books table
		mysqli_set_charset($link, "utf8");
		echo '<i class="fi-check green"></i> Database connection successful<br />';
		// Check if there is already a db table
		$val = mysqli_query($link, "SELECT 1 from db LIMIT 1");
		if($val == false) {
			$sql = "CREATE TABLE `db` (`db_id` int(11) NOT NULL, `db_key` varchar(32) NOT NULL, `db_value` varchar(64) NOT NULL ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
			if(!mysqli_query($link, $sql)) {
				echo mysqli_error($link);
			}
			$sql = "INSERT INTO `db` (`db_id`, `db_key`, `db_value`) VALUES (1, 'version_remote', ''), (2, 'version_last_check', '');";
			if(!mysqli_query($link, $sql)) {
				echo mysqli_error($link);
			}
			$sql = "ALTER TABLE `db` ADD PRIMARY KEY (`db_id`);";
			if(!mysqli_query($link, $sql)) {
				echo mysqli_error($link);
			}
			$sql = "ALTER TABLE `db` MODIFY `db_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;";
			if(!mysqli_query($link, $sql)) {
				echo mysqli_error($link);
			}
		}
		// Check if there is already a books table
		$val = mysqli_query($link, "SELECT 1 from books LIMIT 1");
		if($val !== false) {
		   // Do not overwrite database and books, we are finished
			echo '<i class="fi-alert orange"></i> There is already a database named \'books\' that we are not going to overwrite. The copying of example books will also be skipped. If you already had a previous installation of phpSEL, this is a good thing.<br />';
		} else {
			// Create the database
			$sql = "CREATE TABLE `books` (`books_id` int(11) NOT NULL, `books_time` bigint(14) DEFAULT NULL, `books_author` varchar(255) NOT NULL, `books_title` varchar(255) NOT NULL, `books_title_original` varchar(255) NOT NULL, `books_genre` varchar(64) NOT NULL, `books_world` varchar(64) NOT NULL, `books_world_number` int(11) NOT NULL, `books_series` varchar(64) NOT NULL, `books_series_number` int(11) NOT NULL, `books_publisher` varchar(64) NOT NULL, `books_year` char(4) NOT NULL, `books_year_first` char(4) NOT NULL, `books_language` varchar(16) NOT NULL, `books_already_read` tinyint(1) NOT NULL, `books_physical_copy` tinyint(1) NOT NULL, `books_reading_list` tinyint(1) NOT NULL, `books_wishlist` tinyint(1) NOT NULL, `books_synopsis` text NOT NULL, `books_missing` tinyint(1) NOT NULL, `books_missing_cover` tinyint(1) NOT NULL ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
			if(mysqli_query($link, $sql)) {
				echo '<i class="fi-check green"></i> Creating database table successful<br />';
			} else {
				echo mysqli_error($link);
			}
			// Add primary key
			$sql = "ALTER TABLE `books` ADD PRIMARY KEY (`books_id`);";
			if(!mysqli_query($link, $sql)) {
				echo mysqli_error($link);
			}
			// Set auto increment
			$sql = "ALTER TABLE `books` CHANGE `books_id` `books_id` INT(11) NOT NULL AUTO_INCREMENT;";
			if(!mysqli_query($link, $sql)) {
				echo mysqli_error($link);
			}
			// Insert example data if selected
			if($_SESSION['setup_prepopulate'] == 'yes') {
				$sql = "INSERT INTO `books` (`books_id`, `books_time`, `books_author`, `books_title`, `books_title_original`, `books_genre`, `books_world`, `books_world_number`, `books_series`, `books_series_number`, `books_publisher`, `books_year`, `books_year_first`, `books_language`, `books_already_read`, `books_physical_copy`, `books_reading_list`, `books_wishlist`, `books_synopsis`, `books_missing`, `books_missing_cover`) VALUES (1, 20180813190715, 'Arthur Conan Doyle', 'A Study in Scarlet', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 1, '', '', '1887', 'English', 0, 0, 0, 0, '\'There\'s a scarlet thread of murder running through the colourless skein of life, and our duty is to unravel it, and isolate it, and expose every inch of it.\'\r\n\r\nFrom the moment Dr John Watson takes lodgings in Baker Street with the consulting detective Sherlock Holmes, he becomes intimately acquainted with the bloody violence and frightening ingenuity of the criminal mind.\r\n\r\nIn A Study in Scarlet , Holmes and Watson\'s first mystery, the pair are summoned to a south London house where they find a dead man whose contorted face is a twisted mask of horror. The body is unmarked by violence but on the wall a mysterious word has been written in blood.\r\n\r\nThe police are baffled by the crime and its circumstances. But when Sherlock Holmes applies his brilliantly logical mind to the problem he uncovers a tragic tale of love and deadly revenge . . .', 0, 0), (2, 20180813190843, 'Arthur Conan Doyle', 'The Sign of the Four', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 2, '', '', '1890', 'English', 0, 0, 0, 0, '\'You are a wronged woman and shall have justice. Do not bring police. If you do, all will be in vain. Your unknown friend.\'\r\n\r\nWhen a beautiful young woman is sent a letter inviting her to a sinister assignation, she immediately seeks the advice of the consulting detective Sherlock Holmes.\r\n\r\nFor this is not the first mysterious item Mary Marston has received in the post. Every year for the last six years an anonymous benefactor has sent her a large lustrous pearl. Now it appears the sender of the pearls would like to meet her to right a wrong.\r\n\r\nBut when Sherlock Holmes and his faithful sidekick Watson, aiding Miss Marston, attend the assignation, they embark on a dark and mysterious adventure involving a one-legged ruffian, some hidden treasure, deadly poison darts and a thrilling race along the River Thames.', 0, 0), (3, 20180813191029, 'Arthur Conan Doyle', 'The Adventures of Sherlock Holmes', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 3, '', '', '1892', 'English', 0, 0, 0, 0, '\'It is an old maxim of mine that when you have excluded the impossible, whatever remains, however improbable, must be the truth.\'\r\n\r\nSherlock Holmes, scourge of criminals everywhere, whether they be lurking in London\'s foggy backstreets or plotting behind the walls of an idyllic country mansion, and his faithful colleague Dr Watson solve twelve breathtaking and perplexing mysteries.\r\n\r\nIn The Adventures of Sherlock Holmes, the first collection of the great consulting detective\'s cases, we encounter some of his most famous and devilishly difficult problems, including A Scandal in Bohemia, The Speckled Band, The Red-Headed League, The Blue Carbuncle, The Five Orange Pips and The Man with the Twisted Lip.', 0, 0), (4, 20180813191145, 'Arthur Conan Doyle', 'The Memoirs of Sherlock Holmes', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 4, '', '', '1893', 'English', 0, 0, 0, 0, '\'If I were assured of your eventual destruction I would, in the interests of the public, cheerfully accept my own.\'\r\n\r\nIn The Memoirs of Sherlock Holmes, the consulting detective\'s notoriety as the arch-despoiler of the schemes concocted by the criminal underworld at last gets the better of him.\r\n\r\nThough Holmes and his faithful sidekick Dr Watson solve what will become some of their most bizarre and extraordinary cases - the disappearance of the race horse Silver Blaze, the horrific circumstances of the Greek Interpreter and the curious mystery of the Musgrave Ritual among them - a criminal mastermind is plotting the downfall of the great detective.\r\n\r\nHalf-devil, half-genius, Professor Moriarty leads Holmes and Watson on a grisly cat-and-mouse chase through London and across Europe, culminating in a frightful struggle which will turn the legendary Reichenbach Falls into a water double-grave . . .', 0, 0), (5, 20180813191525, 'Arthur Conan Doyle', 'The Hound of the Baskervilles', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 5, '', '', '1902', 'English', 0, 0, 0, 0, '\'Mr. Holmes, they were the footprints of a gigantic hound!\'\r\n\r\nThe death, quite suddenly, of Sir Charles Baskerville in mysterious circumstances is the trigger for one of the most extraordinary cases ever to challenge the brilliant analytical mind of Sherlock Holmes. As rumours of a legendary hound said to haunt the Baskerville family circulate, Holmes and Watson are asked to ensure the protection of Sir Charles\' only heir, Sir Henry - who has travelled all the way from America to reside at Baskerville Hall in Devon. And it is there, in an isolated mansion surrounded by mile after mile of wild moor, that Holmes and Watson come face to face with a terrifying evil that reaches out from centuries past . . .', 0, 0), (6, 20180813191616, 'Arthur Conan Doyle', 'The Return of Sherlock Holmes', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 6, '', '', '1905', 'English', 0, 0, 0, 0, '\'Holmes,\' I cried. \'Is it really you? Can it indeed be that you are alive? Is it possible that you succeeded in climbing out of that awful abyss?\'\r\n\r\nMissing, presumed dead, for three years, Sherlock Holmes returns triumphantly to his dear companion Dr Watson. And not before time! London has never been in more need of his extraordinary services: a murderous individual with an air gun stalks the city.\r\n\r\nAmong thirteen further brilliant tales of mystery, detection and deduction, Sherlock Holmes investigates the problem of the Norwood Builder, deciphers the message of the Dancing Men, and cracks the case of the Six Napoleons.', 0, 0), (7, 20180813192517, 'Arthur Conan Doyle', 'The Valley of Fear', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 7, '', '', '1915', 'English', 0, 0, 0, 0, '\'There should be no combination of events for which the wit of man cannot conceive an explanation.\'\r\n\r\nIn this tale drawn from the note books of Dr Watson, the deadly hand of Professor Moriarty once more reaches out to commit a vile and ingenious crime. However, a mole in Moriarty\'s frightening criminal organization alerts Sherlock Holmes of the evil deed by means of a cipher.\r\n\r\nWhen Holmes and Watson arrive at a Sussex manor house they appear to be too late. The discovery of a body suggests that Moriarty\'s henchmen have been at their work. But there is much more to this tale of murder than at first meets the eye and Sherlock Holmes is determined to get to the bottom of it.', 0, 0), (8, 20180813192659, 'Arthur Conan Doyle', 'His Last Bow', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 8, '', '', '1917', 'English', 0, 0, 0, 0, '\'We must fall back upon the old axiom that when all other contingencies fail, whatever remains, however improbable, must be the truth.\'\r\n\r\nSherlock Holmes\'s fearless chronicler Dr Watson once again opens his notebooks to bring to light eight further tales of some of the strangest and most fascinating cases to come before the enquiring mind of London\'s most famous detective.\r\n\r\nThese mysteries involve the disappearance of secret plans as well as of a lady of noble standing; the curious circumstances of Wisteria Lodge and of the Devil\'s Foot; as well as the story His Last Bow, the last outing of Holmes and Watson on the eve of the First World War.', 0, 0), (9, 20180813192822, 'Arthur Conan Doyle', 'The Case-Book of Sherlock Holmes', '', 'Detective fiction', '', 0, 'Sherlock Holmes', 9, '', '', '1927', 'English', 0, 0, 0, 0, '\'When you have eliminated all which is impossible, then whatever remains, however improbable, must be the truth.\'\r\n\r\nIn this, the final collection of Sherlock Holmes adventures, the intrepid detective and his faithful companion Dr Watson examine and solve twelve cases that puzzle clients, baffle the police and provide readers with the thrill of the chase.\r\n\r\nThese mysteries - involving an illustrious client and a Sussex vampire; the problems of Thor Bridge and of the Lions Mane; a creeping man and the three-gabled house - all test the bravery of Dr Watson and the brilliant mind of Mr Sherlock Homes, the greatest detective we have ever known.', 0, 0);";
				if(mysqli_query($link, $sql)) {
					echo '<i class="fi-check green"></i> Inserting example books to the database successful<br />';
				}
				// Copying example books
				if(isset($_SESSION['setup_prepopulate']) and $_SESSION['setup_prepopulate'] == 'yes') {
					$errors = false;
					// Copy books
					$source_dir = 'data/examples/books';
					$destination_dir = 'data/books';
					$dir_handle = opendir($source_dir);
					while($file = readdir($dir_handle)) {
						if($file != '.' and $file != '..') {
							if (!copy($source_dir.'/'.$file, $destination_dir.'/'.$file)) {
								$errors = true;
							};
						}
					}
					closedir($dir_handle);
					// Copy cover
					$source_dir = 'data/examples/covers';
					$destination_dir = 'data/covers';
					$dir_handle = opendir($source_dir);
					while($file = readdir($dir_handle)) {
						if($file != '.' and $file != '..') {
							if (!copy($source_dir.'/'.$file, $destination_dir.'/'.$file)) {
								$errors = true;
							};
						}
					}
					closedir($dir_handle);
					// Error reporting
					if($errors == true) {
						echo '<i class="fi-alert orange"></i> There was a problem during the copying of the example files! You might have to copy them manually from /data/examples/books to /data/books and from /data/examples/covers to /data/covers.<br />';
					} else {
						echo '<i class="fi-check green"></i> Copying example books successful<br />';
					}
				}

			}
		}
	}
}
// Create the config.php file
$errors_config = false;
if($errors_database == false) {
	$config = @file_get_contents('config-sample.php');
	$config = str_replace('$site_title = \'phpSEL\';', '$site_title = \''.$_SESSION['setup_library_name'].'\';', $config);
	$config = str_replace('$protocol = \'http\';', '$protocol = \''.$_SESSION['setup_protocol'].'\';', $config);
	$config = str_replace('$subdirectory = \'\';', '$subdirectory = \''.$_SESSION['setup_subdirectory'].'\';', $config);
	$config = str_replace('$site_language = \'en\';', '$site_language = \''.$_SESSION['setup_language'].'\';', $config);
	$config = str_replace('$login_username = \'admin\';', '$login_username = \''.$_SESSION['setup_username'].'\';', $config);
	$config = str_replace('$login_password = \'adminpass\';', '$login_password = \''.$_SESSION['setup_password'].'\';', $config);
	$config = str_replace('$db_host = \'localhost\';', '$db_host = \''.$_SESSION['setup_db_host'].'\';', $config);
	$config = str_replace('$db_port = \'\';', '$db_port = \''.$_SESSION['setup_db_port'].'\';', $config);
	$config = str_replace('$db_database = \'database_name_here\';', '$db_database = \''.$_SESSION['setup_db_name'].'\';', $config);
	$config = str_replace('$db_user = \'username_here\';', '$db_user = \''.$_SESSION['setup_db_user'].'\';', $config);
	$config = str_replace('$db_password = \'password_here\';', '$db_password = \''.$_SESSION['setup_db_password'].'\';', $config);

	if($config_file = @fopen("config.php", "w")) {
		fwrite($config_file, $config);
		fclose($config_file);
		echo '<i class="fi-check green"></i> Creating config.php successful<br />';
	} else {
		echo '<i class="fi-alert orange"></i> Creating config.php failed. Please create it yourself and insert the following content before trying to log in:<br />';
		echo '<textarea rows="10">';
		echo $config;
		echo '</textarea>';
	};
};

echo '</p>';
echo '<br />';
echo '<a class="button secondary" href="setup.php?step=4">&laquo; '.lng('back').'</a> ';
echo '<a class="button" href="index.php">'.lng('log_in').' &raquo;</a> ';

echo '</div>';

echo '</div>';
echo '</div>';