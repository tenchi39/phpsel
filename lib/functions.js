function ajaxContent(url, id) {
	var AJAX = null;
	if(window.XMLHttpRequest) {
		AJAX=new XMLHttpRequest();
	} else {
		AJAX=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(AJAX==null) {
		alert("Your browser doesn't support AJAX.");
		return false
	}
	AJAX.onreadystatechange = function() {
		if(AJAX.readyState==4 || AJAX.readyState==200) {
			identity=document.getElementById(id);
		 	identity.innerHTML = '';
			identity.innerHTML = (identity.innerHTML + AJAX.responseText);
		}
	}
	//If ajax doesn't work, change POST to GET!
	AJAX.open("POST", url, true);
	AJAX.send(null);
}

function removeClass(id, name) {
	$('#' + id).removeClass(name);
}

function clearValue(id) {
	$('#' + id).val("");
}

function addValue(id, value) {
	var content = $('#' + id).val();
	if(content == '') {
		content += value;	
	} else {
		content += ', ' + value;
	}
	$('#' + id).val(content);
}