<?php

if(isset($_REQUEST['setup_library_name'])) {
	$_SESSION['setup_library_name'] = $_REQUEST['setup_library_name'];
}
if(isset($_REQUEST['setup_protocol'])) {
	$_SESSION['setup_protocol'] = $_REQUEST['setup_protocol'];
}
if(isset($_REQUEST['setup_subdirectory'])) {
	$_SESSION['setup_subdirectory'] = $_REQUEST['setup_subdirectory'];
}
if(isset($_REQUEST['setup_language'])) {
	$_SESSION['setup_language'] = $_REQUEST['setup_language'];
}
if(isset($_REQUEST['setup_prepopulate'])) {
	$_SESSION['setup_prepopulate'] = $_REQUEST['setup_prepopulate'];
}

$errors = false;
////////////////
if(!isset($_SESSION['setup_library_name']) or $_SESSION['setup_library_name'] == '') {
	$errors = true;
}
////////////////
if($errors == true) {
	header('Location: setup.php?step=2&errors=true');
	exit();
}

echo '<form action="setup.php?step=4" method="POST">';
echo '<div class="grid-container">';
echo '<div class="grid-x grid-padding-x">';

echo '<div class="small-12 cell">';
echo '<h3>Step 3: Login data</h3>';
echo '<p>This will be your user that you can access phpSEL with. Keep it safe and don\'t share it with anyone.</p>';
echo '</div>';

echo '<div class="small-12 cell">';
echo '<label';
if(isset($_REQUEST['errors']) and $_REQUEST['errors'] == true and (!isset($_SESSION['setup_username']) or $_SESSION['setup_username'] == '')) {
	echo ' class="red"';
};
echo '>Username';
echo '<input type="text" name="setup_username" value="';
if(isset($_SESSION['setup_username'])) {
	echo $_SESSION['setup_username'];
} else {
	echo 'admin';
}
echo '">';
echo '</label>';
echo '<p class="help-text">Be sure to change this!</p>';
echo '</div>';

echo '<div class="small-12 cell">';
echo '<label';
if(isset($_REQUEST['errors']) and $_REQUEST['errors'] == true and (!isset($_SESSION['setup_password']) or $_SESSION['setup_password'] == '')) {
	echo ' class="red"';
};
echo '>Password';
echo '<input type="password" name="setup_password" value="';
if(isset($_SESSION['setup_password'])) {
	echo $_SESSION['setup_password'];
} else {
	echo 'adminpass';
}
echo '">';
echo '</label>';
echo '<p class="help-text">Be sure to change this!</p>';
echo '</div>';


echo '<div class="medium-12 cell">';
echo '<br />';
echo '<a class="button secondary" href="setup.php?step=2">&laquo; '.lng('back').'</a> ';
echo '<input type="submit" class="button" value="'.lng('next').' &raquo;">';
echo '</div>';

echo '</div>';
echo '</div>';
echo '</form>';