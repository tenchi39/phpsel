<?php

$i = 0;
$books = array();
foreach ($import_links as $import_link) {
	$books[$i]['link'] = 'https://www.goodreads.com'.$import_link;
	// Get the book page
	$html_book = curlGet($books[$i]['link']);
	//echo '<textarea>';
	//echo $html_book;
	//echo '</textarea>';

	// Authors
	$books[$i]['authors'] = '';
	$import_authors = explode("authorName__container", $html_book);
	foreach($import_authors as $value) {
		$value = strBetween($value, '<span itemprop="name">', '</span>');
		if($books[$i]['authors'] != '') {
			$books[$i]['authors'] .= ', ';
		}
		$books[$i]['authors'] .= $value;
	}
	// Title & Series
	$books[$i]['title'] = trim(strBetween($html_book, '<h1 id="bookTitle" class="gr-h1 gr-h1--serif" itemprop="name">', '</h1>'));
	$books[$i]['series'] = strBetween($html_book, '<h2 id="bookSeries">', '</h2>');
	$books[$i]['series'] = strip_tags($books[$i]['series']);
	$books[$i]['series'] = strBetween($books[$i]['series'], '(', ')');
	$books[$i]['series_number'] = preg_replace("/[^0-9]{1,4}/", '', $books[$i]['series']);
	$books[$i]['series'] = str_replace('#'.$books[$i]['series_number'], '', $books[$i]['series']);
	$books[$i]['series'] = trim($books[$i]['series']);
	// Original title
	$original_title = strBetween($html_book, '<div class="infoBoxRowTitle">Original Title</div>', '</div>');
	$original_title = explode('<div class="infoBoxRowItem">', $original_title);
	if(isset($original_title[1])) {
		$books[$i]['title_original'] = trim($original_title[1]);
	}
	// Genre
	$books[$i]['genre'] = '';
	$genre = explode('<a class="actionLinkLite bookPageGenreLink"', $html_book);
	for($j = 1;$j<=count($genre)-1;$j++) {
		$value = '<a '.$genre[$j];
		$value = explode('</a>', $value);
		$value = $value[0].'</a>';
		$value = strip_tags($value);
		if($books[$i]['genre'] != '') {
			$books[$i]['genre'] .= ', ';
		}
		$books[$i]['genre'] .= $value;
	}
	// Synopsys
	$synopsis = explode('<div id="description" class="readable stacked" style="right:0">', $html_book);
	if(isset($synopsis[1])) {	
		$synopsis = explode('</div>', $synopsis[1]);
		$synopsis = explode('<span ', $synopsis[0]);
		if(count($synopsis) == 2) {
			$synopsis[1] = '<span '.$synopsis[1];
			$books[$i]['synopsis'] = trim(strip_tags(br2nl($synopsis[1])));
		}
		if(count($synopsis) == 3) {
			$synopsis[2] = '<span '.$synopsis[2];
			$books[$i]['synopsis'] = trim(strip_tags(br2nl($synopsis[2])));
		}
	}
	// Covers
	$covers = '';
	$covers = strBetween($html_book, '<img id="coverImage" ', '</a>');
	$covers = strBetween($covers, 'src="', '" />');
	if($covers != '') {
		$books[$i]['covers'][] = $covers;
	}
	// Year of first publication
	$books[$i]['year_of_first_publication'] = strBetween($html_book, '(first published ', ')');
	$books[$i]['year_of_first_publication'] = substr($books[$i]['year_of_first_publication'], -4);
	// Year of publication
	$books[$i]['year_of_publication'] = strBetween($html_book, '<div class="row">
            Published', '</div>');
	$books[$i]['year_of_publication'] = preg_replace("/[^0-9]{1,4}/", '', $books[$i]['year_of_publication']);
	$books[$i]['year_of_publication'] = substr($books[$i]['year_of_publication'], -4);
	$i++;
}

?>