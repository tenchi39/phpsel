<?php

echo '<form action="setup.php?step=3" method="POST">';
echo '<div class="grid-container">';
echo '<div class="grid-x grid-padding-x">';

echo '<div class="small-12 cell">';
echo '<h3>Step 2: Basic settings</h3>';
echo '<p>These are the basic settings for phpSEL.</p>';
echo '</div>';

echo '<div class="small-12 cell">';
echo '<label';
if(isset($_REQUEST['errors']) and $_REQUEST['errors'] == true and (!isset($_SESSION['setup_library_name']) or $_SESSION['setup_library_name'] == '')) {
	echo ' class="red"';
};
echo '>Library name';
echo '<input type="text" name="setup_library_name" value="';
if(isset($_SESSION['setup_library_name'])) {
	echo $_SESSION['setup_library_name'];
} else {
	echo 'phpSEL';
}
echo '">';
echo '</label>';
echo '<p class="help-text">This defines the name of your library and will appear on the browser tab you are running phpSEL in. Give it a unique name to make it more personal.</p>';
echo '</div>';

echo '<div class="medium-12 cell">';
echo '<label>Protocol';
echo '<select name="setup_protocol">';
echo '<option value="http"';
if(isset($_SESSION['setup_protocol']) and $_SESSION['setup_protocol'] == 'http') {
	echo ' selected="selected"';
} 
echo '>http</option>';
echo '<option value="https"';
if(isset($_SESSION['setup_protocol']) and $_SESSION['setup_protocol'] == 'https') {
	echo ' selected="selected"';
} else {
	if(isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] != '') {
		echo ' selected="selected"';
	}
}
echo '>https</option>';
echo '</select>';
echo '</label>';
echo '<p class="help-text">Setup will try to auto-detect the proper connection protocol, but you can overwrite it here if you are not satisfied with the result.</p>';
echo '</div>';


echo '<div class="medium-12 cell">';
$subdir = '';
$subdirectory = explode("/", $_SERVER['PHP_SELF']);
for($i=1;$i<(count($subdirectory)-1);$i++) {
	$subdir .= '/';
	$subdir .= $subdirectory[$i];
};
echo '<label>Subdirectory <small>(optional)</small>';
echo '<input type="text" name="setup_subdirectory" value="';
if(isset($_SESSION['setup_subdirectory'])) {
	echo $_SESSION['setup_subdirectory'];
} else {
	echo $subdir;
}
echo '">';
echo '</label>';
echo '<p class="help-text">Setup will try to auto-detect if phpSEL is in a subdirectory or not. If you plan to use phpSEL directly under a domain name like somedomain.com, leave this field empty. If you will put it in a subdirectory like somedomain.com/mylibrary, put the subdirectory part with the leading slash here.</p>';
echo '</div>';

echo '<div class="medium-12 cell">';

echo '<label>Language';
echo '<select name="setup_language">';
include('lng/language_codes.php');
$dir_handle = opendir('lng');
while($file = readdir($dir_handle)) {
	if($file != '.' and $file != '..' and $file != 'language_codes.php') {
		$file = explode(".", $file);
		echo '<option value="'.$file[0].'"';
		if(isset($_SESSION['setup_language'])) {
			if($_SESSION['setup_language'] == $file[0]) {
				echo ' selected="selected"';
			}
		} else {
			if($file[0] == 'en') {
				echo ' selected="selected"';
			}
		}
		if($language_codes[$file[0]] == '') {
			$language_codes[$file[0]] = $file[0];
		}
		echo '>'.$language_codes[$file[0]].'</option>';
	}
}
closedir($dir_handle);
echo '</select>';
echo '</label>';
echo '<p class="help-text">You can make your own translation by making a copy of any language file in /lng and editing it. You will also need to add the language code to the /lng/language_codes.php file. You can do this later and change the language in config.php.</p>';
echo '</div>';

echo '<div class="medium-12 cell">';
echo '<label>Copy example books';
echo '<select name="setup_prepopulate">';
echo '<option value="yes"';
if(isset($_SESSION['setup_prepopulate']) and ($_SESSION['setup_prepopulate'] == 'yes')) {
	echo ' selected="selected"';
}
echo '>Yes</option>';
echo '<option value="no"';
if(isset($_SESSION['setup_prepopulate']) and ($_SESSION['setup_prepopulate'] == 'no')) {
	echo ' selected="selected"';
}
echo '>No</option>';
echo '</select>';
echo '</label>';
echo '<p class="help-text">Setup can copy a few public domain books into your library, so you can see how things work in phpSEL.</p>';
echo '</div>';


echo '<div class="medium-12 cell">';
echo '<br />';
echo '<a class="button secondary" href="setup.php?step=1">&laquo; '.lng('back').'</a> ';
echo '<input type="submit" class="button" value="'.lng('next').' &raquo;">';
echo '</div>';

echo '</div>';
echo '</div>';
echo '</form>';