<?php
	if($_SESSION['guest_session'] == true) {
		echo unauthorizedMessage();
		return;
	}
?>
<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12">
			<br />
			<h2><?php echo lng('import'); ?></h2>
			<fieldset class="fieldset">
  			<legend><?php echo lng('search'); ?></legend>
				<form action="index.php?view=import" method="POST">
				<input type="hidden" name="submitted" value="true">
				<div class="grid-x grid-margin-x">
					<div class="cell small-12 medium-3 large-2">
						<select name="import_source">
							<option disabled="disabled"<?php if(!isset($_REQUEST['import_source'])){ echo ' selected="selected"';}?>>Select source</option>
							<option value="goodreads"<?php if(isset($_REQUEST['import_source']) and $_REQUEST['import_source'] == 'goodreads'){ echo ' selected="selected"';}?>>goodreads.com</option>
							<option value="moly"<?php if(isset($_REQUEST['import_source']) and $_REQUEST['import_source'] == 'moly'){ echo ' selected="selected"';}?>>moly.hu</option>
						</select>
					</div>
					<div class="cell small-12 medium-6 large-8">
						<input class="input-group-field" type="text" id="import_titleauthor" name="import_titleauthor" placeholder="<?php echo lng('search_placeholder'); ?>" value="<?php if(isset($_REQUEST['import_titleauthor'])){echo $_REQUEST['import_titleauthor'];}?>">
					</div>
					<div class="cell small-12 medium-3 large-2">
						<div class="hide-for-medium">
							<br>
						</div>
						<input type="submit" class="button expanded" value="<?php echo lng('search');?>" onclick="removeClass('search_in_prgress_text', 'hide');" />
						
					</div>
				</div>
				</form>
			</fieldset>
			<div class="search_in_progress">
			<span id="search_in_prgress_text" class="hide"><img src="img/spinner.gif" alt=""><?php echo lng('search_in_progress'); ?></span>
			</div>

			<?php
				
				if(isset($_REQUEST['import_source']) and $_REQUEST['import_source'] != '' and isset($_REQUEST['import_titleauthor']) and $_REQUEST['import_titleauthor'] != '') {
					// Get the book list
					include('lib/import_'.$_REQUEST['import_source'].'_list.php');
					// Get the book data
					include('lib/import_'.$_REQUEST['import_source'].'_book.php');

					if(!empty($books)) {
						echo '<h3>'.lng('results').'</h3>';
						foreach ($books as $key => $book) {
							/////////////////////////////6
							echo '<h4>#'.($key+1).' '.$books[$key]['authors'].': '.$books[$key]['title'].'</h4>';

							echo '<form enctype="multipart/form-data" action="index.php?view=form" method="POST">';
							echo '<table class="unstriped hover stack">';
							echo '<tr>';
							echo '<td class="nowrap">';
							echo lng('source');
							echo '</td>';
							echo '<td>';
							echo '<a href="'.$books[$key]['link'].'" target="_blank">'.$books[$key]['link'].'</div>';
							echo '</td>';
							echo '</tr>';
							echo '<td class="nowrap">';
							echo '<b>'.lng('title').':</b>';
							echo '</td>';
							echo '<td>';
							echo $books[$key]['title'];
							echo '</td>';
							echo '</tr>';
							$result = mysqli_query($link, "SELECT * FROM books WHERE books_title='".mysqli_real_escape_string($link, $books[$key]['title'])."'");
							$already_in_the_library = '';
							while($myrow = mysqli_fetch_assoc($result)) {
								$already_in_the_library .= '<a href="index.php?view=details&id='.$myrow['books_id'].'" target="_blank">'.$myrow['books_author'].': '.$myrow['books_title'].'</a><br>';
							}
							if($already_in_the_library != '') {
								echo '<tr>';
								echo '<td class="nowrap red">';
								echo '<b>'.lng('already_in_the_library').':</b>';
								echo '</td>';
								echo '<td>';
								echo $already_in_the_library;
								echo '</td>';
								echo '</tr>';
							}
							if(isset($books[$key]['title_original']) and $books[$key]['title_original'] != '' and $books[$key]['title_original'] != $books[$key]['title']) {
								echo '<tr>';
								echo '<td class="nowrap">';
								echo '<b>'.lng('title_original').':</b>';
								echo '</td>';
								echo '<td>';
								echo $books[$key]['title_original'];
								echo '</td>';
								echo '</tr>';
							}
							echo '<tr>';
							echo '<td class="nowrap">';
							echo '<b>'.lng('author').':</b>';
							echo '</td>';
							echo '<td>';
							echo $books[$key]['authors'];
							echo '</td>';
							echo '</tr>';
							if(isset($books[$key]['series']) and $books[$key]['series'] != '') {
								echo '<tr>';
								echo '<td class="nowrap">';
								echo '<b>'.lng('series').':</b>';
								echo '</td>';
								echo '<td>';
								echo $books[$key]['series'];
								if(isset($books[$key]['series_number']) and $books[$key]['series_number'] != '') {
									echo ' #'.$books[$key]['series_number'];
								}
								echo '</td>';
								echo '</tr>';
							}
							if(isset($books[$key]['genre']) and $books[$key]['genre'] != '') {
								echo '<tr>';
								echo '<td class="nowrap">';
								echo '<b>'.lng('genre').':</b>';
								echo '</td>';
								echo '<td>';
								echo $books[$key]['genre'];
								echo '</td>';
								echo '</tr>';
							}
							if(isset($books[$key]['year_of_publication']) and $books[$key]['year_of_publication'] != '') {
								echo '<tr>';
								echo '<td class="nowrap">';
								echo '<b>'.lng('year_of_publication').':</b>';
								echo '</td>';
								echo '<td>';
								echo $books[$key]['year_of_publication'];
								echo '</td>';
								echo '</tr>';
							}
							if(isset($books[$key]['year_of_first_publication']) and $books[$key]['year_of_first_publication'] != '') {
								echo '<tr>';
								echo '<td class="nowrap">';
								echo '<b>'.lng('year_of_first_publication').':</b>';
								echo '</td>';
								echo '<td>';
								echo $books[$key]['year_of_first_publication'];
								echo '</td>';
								echo '</tr>';
							}
							if(isset($books[$key]['synopsis']) and $books[$key]['synopsis'] != '') {
								echo '<tr>';
								echo '<td class="nowrap">';
								echo '<b>'.lng('synopsis').':</b>';
								echo '</td>';
								echo '<td>';
								echo nl2br($books[$key]['synopsis']);
								echo '</td>';
								echo '</tr>';
							}
						
							echo '<div class="hide">';
							echo '<input name="title" type="text" value="'.$books[$key]['title'].'">';
							echo '<input name="title_original" type="text" value="'.$books[$key]['title_original'].'">';
							echo '<input name="author" type="text" value="'.$books[$key]['authors'].'">';
							echo '<input name="series" type="text" value="'.$books[$key]['series'].'">';
							echo '<input name="series_number" type="text" value="'.$books[$key]['series_number'].'">';
							echo '<input name="genre" type="text" value="'.$books[$key]['genre'].'">';
							echo '<input name="year_of_publication" type="text" value="'.$books[$key]['year_of_publication'].'">';
							echo '<input name="year_of_first_publication" type="text" value="'.$books[$key]['year_of_first_publication'].'">';
							echo '<textarea name="synopsis" class="height250px">';
							echo $books[$key]['synopsis'];
							echo '</textarea>';
							echo '</div>'; // End of .hide
							// Book covers
							if(!empty($books[$key]['covers'])) {
								echo '<tr>';
								echo '<td class="nowrap">';
								echo '<b>'.lng('cover').':</b>';
								echo '</td>';
								echo '<td class="nowrap overflowxauto">';

								$first = true;
								foreach ($books[$key]['covers'] as $cover) {
									if($first == true) {
										$first = false;
										echo '<input type="radio" name="import_cover" value="'.$cover.'" id="'.$cover.'" checked="checked">';
									} else {
										echo '<input type="radio" name="import_cover" value="'.$cover.'" id="'.$cover.'">';
									};
									echo '<label for="'.$cover.'">';
									echo '<img src="'.$cover.'" alt="" class="height250px" />';
									echo '</label>';
								}
								echo '</td>';
								echo '</tr>';								
							}
							echo '</table>';
							// Submit
							echo '<input type="submit" class="button" value="'.lng('import_to_library').'" />';
							echo '</form>';
							echo '<br />';
							echo '<br />';
						}
					} else {
						echo '<div class="callout warning">';
						echo lng('your_search_yielded_no_results');
						echo '</div>';
					}
				} else {
					if(isset($_REQUEST['submitted'])) {
						echo '<div class="callout alert">';
						echo lng('select_source_and_enter_a_search_term');
						echo '</div>';
					} else {
						echo '<p>'.lng('import_description').'</p>';
					}
				}

			?>
			<script type="text/javascript">
				document.getElementById("import_titleauthor").focus();
			</script>
		</div>
	</div>
</div>