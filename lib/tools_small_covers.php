<?php
	if($_SESSION['guest_session'] == true) {
		echo unauthorizedMessage();
		return;
	}
?>
<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12">
			<br />
				<h2><?php echo lng('small_covers'); ?></h2>
				<p><?php echo lng('small_covers_description'); ?> (<?php echo $list_image_max_height; ?>px)</p>
				<h3><?php echo lng('results'); ?></h3>

				<?php
					$text = '';
					$result = mysqli_query($link, "SELECT books_id, books_author, books_title FROM books ORDER BY books_id");
					while($myrow = mysqli_fetch_assoc($result)) {
						$cover = getFilename('data/covers', $myrow['books_id'].'-13-');
						if($cover != '') {
							$size = getimagesize($cover);
							if($size[1] < $list_image_max_height) {
								$text .= '<li>';
								if($myrow['books_author'] != '') {
									$text .= $myrow['books_author'].': ';
								}
								$text .= '<a href="index.php?view=details&id='.$myrow['books_id'].'">'.$myrow['books_title'].'</a> <small>('.$size[0].'x'.$size[1].'px)</small></li>';
							}
						}
					}

					if($text != '') {
						echo '<ol>';
						echo $text;
						echo '</ol>';
					} else {
						echo '<div class="callout warning">';
						echo lng('your_search_yielded_no_results');
						echo '</div>';
					}
				?>

			<br>
		</div>
	</div>
</div>