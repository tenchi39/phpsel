<br />
<?php

	$search_array = array('title', 'title_original', 'author', 'language', 'world', 'series', 'genre', 'publisher', 'year', 'year_first');
	$search_array_lng = array(
			'title' => lng('title'), 
			'title_original' => lng('title_original'), 
			'author' => lng('author'), 
			'language' => lng('language'), 
			'world' => lng('world'), 
			'series' => lng('series'), 
			'genre' => lng('genre'), 
			'publisher' => lng('publisher'), 
			'year' => lng('year_of_publication'), 
			'year_first' => lng('year_of_first_publication')
		);
	$search_array_binary = array('reading_list', 'wishlist', 'already_read', 'physical_copy', 'missing', 'missing_cover');
	$search_array_binary_lng = array(
		'reading_list' => lng('on_my_reading_list'), 
		'wishlist' => lng('on_my_wishlist'), 
		'already_read' => lng('already_read'), 
		'physical_copy' => lng('physical_copy'), 
		'missing' => lng('missing_book'), 
		'missing_cover' => lng('missing_cover')
	);

	// Clear search
	if(isset($_REQUEST['clear_search'])) {
		foreach($search_array as $search_key) {
			unset($_SESSION['search_'.$search_key]);
		}
		foreach($search_array_binary as $search_key) {
			unset($_SESSION['search_'.$search_key]);
		}
	}
	foreach($search_array as $search_key) {
		if(isset($_REQUEST['clear_search_'.$search_key])) {
			unset($_SESSION['search_'.$search_key]);
		}
	}
	foreach($search_array_binary as $search_key) {
		if(isset($_REQUEST['clear_search_'.$search_key])) {
			unset($_SESSION['search_'.$search_key]);
		}
	}
	
	// Delete book
	if(isset($_REQUEST['delete_id']) and $_REQUEST['delete_id'] != '' and $_SESSION['guest_session'] == false) {
		mysqli_query($link, "DELETE FROM books WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['delete_id'])."' LIMIT 1");
		if($demo_mode == false) {
			$filename = getFilename('data/covers', $_REQUEST['delete_id'].'-13-');
			if($filename != '') {
				unlink($filename);
			}
			$filename = getFilename('data/books', $_REQUEST['delete_id'].'-14-');
			if($filename != '') {
				unlink($filename);
			}
			$filename = getFilename('data/books', $_REQUEST['delete_id'].'-15-');
			if($filename != '') {
				unlink($filename);
			}
			$filename = getFilename('data/books', $_REQUEST['delete_id'].'-16-');
			if($filename != '') {
				unlink($filename);
			}
			$filename = getFilename('data/books', $_REQUEST['delete_id'].'-17-');
			if($filename != '') {
				unlink($filename);
			}
		}
	}

	$query = '';
	$search_extralink = '';
	$extraquery = '';

	////////////////
	// ABC filter //
	////////////////

	$abc_extralink = '';
	if(!empty($_REQUEST['abc'])) {
		if($_REQUEST['abc'] == '0-9') {
			$extraquery .= 'AND books_title ';
		} else {
			$extraquery .= "AND books_title LIKE '".$_REQUEST['abc']."%'";
		}
		$abc_extralink .= '&amp;abc='.$_REQUEST['abc'];
	}

	//////////////
	// Order by //
	//////////////

	$order_by_extralink = '';
	if(!empty($_REQUEST['order'])) {
		if($_REQUEST['order'] == 'id') {
			$order_by = 'books_id DESC';
			$order_by_extralink .= '&amp;order=id';
		}
		if($_REQUEST['order'] == 'title') {
			$order_by = 'books_title';
			$order_by_extralink .= '&amp;order=title';
		}
		if($_REQUEST['order'] == 'title_original') {
			$order_by = 'books_title_original';
			$extraquery .= "AND books_title_original!='' ";
			$order_by_extralink .= '&amp;order=title_original';
		}
		if($_REQUEST['order'] == 'author') {
			$order_by = 'books_author';
			$extraquery .= "AND books_author!='' ";
			$order_by_extralink .= '&amp;order=author';
		}
		if($_REQUEST['order'] == 'genre') {
			$order_by = 'books_genre';
			$order_by_extralink .= '&amp;order=genre';
		}
		if($_REQUEST['order'] == 'world') {
			$order_by = 'books_world';
			$extraquery .= "AND books_world!='' ";
			$order_by_extralink .= '&amp;order=world';
		}
		if($_REQUEST['order'] == 'series') {
			$order_by = 'books_series';
			$extraquery .= "AND books_series!='' ";
			$order_by_extralink .= '&amp;order=series';
		}
		if($_REQUEST['order'] == 'publisher') {
			$order_by = 'books_publisher';
			$extraquery .= "AND books_publisher!='' ";
			$order_by_extralink .= '&amp;order=publisher';
		}
		if($_REQUEST['order'] == 'year') {
			$order_by = 'books_year';
			$extraquery .= "AND books_year!='' ";
			$order_by_extralink .= '&amp;order=year';
		}
		if($_REQUEST['order'] == 'year_first') {
			$order_by = 'books_year_first';
			$extraquery .= "AND books_year_first!='' ";
			$order_by_extralink .= '&amp;order=year_first';
		}
	} else {
		//echo 'aaa';
		//$_REQUEST['order'] = $list_default_order;
		//echo $_REQUEST['order'];
		$_REQUEST['order'] = $list_default_order;
		$order_by = 'books_'.$list_default_order.' '.$list_default_direction;
		$order_by_extralink .= '&amp;order='.$list_default_order;
		//echo $order_by;
	}

	////////////
	// Search //
	////////////

	if($search_extralink == '' and isset($_REQUEST['search']) and $_REQUEST['search'] == 'true') {
		$search_extralink .= '&amp;search=true';
	}

	if(!isset($_REQUEST['titleauthor']) or $_REQUEST['titleauthor'] == '') {
		$_REQUEST['titleauthor'] = '';
	} else {
		if($query == '') {
			$query .= " (books_title LIKE '%".mysqli_real_escape_string($link, $_REQUEST['titleauthor'])."%' OR books_author LIKE '%".mysqli_real_escape_string($link, $_REQUEST['titleauthor'])."%' COLLATE utf8_bin)";
		} else {
			$query .= " AND (books_title LIKE '%".mysqli_real_escape_string($link, $_REQUEST['titleauthor'])."%' OR books_author LIKE '%".mysqli_real_escape_string($link, $_REQUEST['titleauthor'])."%' COLLATE utf8_bin)";
		}
		$search_extralink .= '&amp;titleauthor='.$_REQUEST['titleauthor'];
	}

	foreach($search_array as $search_key) {
		if((!isset($_REQUEST[$search_key]) or $_REQUEST[$search_key] == '') and (!isset($_SESSION['search_'.$search_key]) or $_SESSION['search_'.$search_key] == '')) {
			$_REQUEST[$search_key] = '';
		} else {
			if((isset($_REQUEST[$search_key]) and $_REQUEST[$search_key] == 'N/A') or (isset($_SESSION['search_'.$search_key]) and $_SESSION['search_'.$search_key] == 'N/A')) {
				$_SESSION['search_'.$search_key] = 'N/A';
				if(isset($_REQUEST['search_'.$search_key])) {
					$_SESSION['search_'.$search_key] = 'N/A';
				} else {
					$_REQUEST[$search_key] = $_SESSION['search_'.$search_key];
				}
				if($query == '') {
					$query .= " books_$search_key=''";
				} else {
					$query .= " AND books_$search_key=''";
				}
			/*} elseif($search_key == 'genre' and (isset($_REQUEST['genre']) and $_REQUEST['genre'] == 'N/A') or (isset($_SESSION['search_genre']) and $_SESSION['search_genre'] == 'N/A')) {
				$_SESSION['search_genre'] = 'N/A';
				if(isset($_REQUEST['search_genre'])) {
					$_SESSION['search_genre'] = 'N/A';
				} else {
					$_REQUEST['genre'] = $_SESSION['search_genre'];
				}
				if($query == '') {
					$query .= " books_genre=''";
				} else {
					$query .= " AND books_genre=''";
				}*/
			} else {
				if(isset($_REQUEST[$search_key])) {
					$_SESSION['search_'.$search_key] = $_REQUEST[$search_key];
				} else {
					$_REQUEST[$search_key] = $_SESSION['search_'.$search_key];
				}
				if($query == '') {
					$query .= " books_$search_key LIKE '%".mysqli_real_escape_string($link, $_REQUEST[$search_key])."%' COLLATE utf8_bin";
				} else {
					$query .= " AND books_$search_key LIKE '%".mysqli_real_escape_string($link, $_REQUEST[$search_key])."%' COLLATE utf8_bin";
				}
			}
		}
	}

	foreach($search_array_binary as $search_key) {
		if((!isset($_REQUEST[$search_key]) or $_REQUEST[$search_key] == '') and (!isset($_SESSION['search_'.$search_key]) or $_SESSION['search_'.$search_key] == '')) {
			$_REQUEST[$search_key] = '';
		} else {
			if(isset($_REQUEST[$search_key])) {
				$_SESSION['search_'.$search_key] = $_REQUEST[$search_key];
			} else {
				$_REQUEST[$search_key] = $_SESSION['search_'.$search_key];
			}
			if($query == '') {
				if($_REQUEST[$search_key] == '0') {
					$query .= " books_$search_key=0";
				}
				if($_REQUEST[$search_key] == '1') {
					$query .= " books_$search_key=1";
				}
			} else {
				if($_REQUEST[$search_key] == '0') {
					$query .= " AND books_$search_key=0";
				}
				if($_REQUEST[$search_key] == '1') {
					$query .= " AND books_$search_key=1";
				}
			}
		}
	}

	////////////////
	// Pagination //
	////////////////

	$pagerdiv = '';
	$pager['page'] = 1;

	// Lapozó
	if(!empty($_REQUEST['page'])) {
		$pager['page'] = $_REQUEST['page'];
	}
	if(empty($pager['items'])) {
		$pager['items'] = $list_items;
	}
	if($query == '') {
		$query = "books_id=books_id";
	}
	$pager['total'] = countMysqlItems('books', "WHERE $query $extraquery");
	//echo 'total: ';
	//echo $pager['total'];
	//echo '<br />';
	$pager['pages'] = ceil($pager['total']/$pager['items']);
	//echo 'pages: ';
	//echo $pager['pages'];
	//echo '<br />';
	$pager['remnant'] = $pager['total']%$pager['items'];
	//echo 'remnant: ';
	//echo $pager['remnant'];
	//echo '<br />';
	$pager['first'] = ($pager['page']*$pager['items'])-$pager['items'];
	//echo 'first: ';
	//echo $pager['first'];
	//echo '<br />';
	if($pager['page'] == $pager['pages']) {
		$pager['last'] = $pager['remnant'];
	} else {
		$pager['last'] = $pager['page']*$pager['items'];
	}
	//echo 'last: ';
	//echo $pager['last'];
	//echo '<br />';
	if(($pager['page']-3)>0) {
		$i_start = $pager['page']-3;
	} else {
		$i_start = 1;
	}
	//echo $i_start;
	//echo '<br />';
	if(($pager['page']+3)<$pager['pages']) {
		$i_end = $pager['page']+3;
	} else {
		$i_end = $pager['pages'];
	}
	//echo $i_end;
	$text_pager = '';
	if($pager['total'] > 0 and $pager['pages'] > 1) {
		if($pager['page'] == 1) {
			$pagerdiv .= ' &lt;&lt;';
			$pagerdiv .= ' | &lt;';
		} else {
			$pagerdiv .= ' <a href="index.php?page=list&amp;page=1'.$order_by_extralink.$abc_extralink.$search_extralink.'">&lt;&lt;</a>';
			$pagerdiv .= ' | <a id="previous_page_link" href="index.php?page=list&amp;page='.($pager['page']-1).$order_by_extralink.$abc_extralink.$search_extralink.'">&lt;</a>';
		}
		for($i=$i_start;$i<=$i_end;$i++) {
			if($i == $pager['page']) {
				$pagerdiv .= ' | <b>'.$i.'</b>';
			} else {
				$pagerdiv .= ' | <a href="index.php?page=list&amp;page='.$i.$order_by_extralink.$abc_extralink.$search_extralink.'">'.$i.'</a>';
			}
		}
		if($pager['page'] == $pager['pages']) {
			$pagerdiv .= ' | &gt;';
			$pagerdiv .= ' | &gt;&gt;';
		} else {
			$pagerdiv .= ' | <a id="next_page_link" href="index.php?page=list&amp;page='.($pager['page']+1).$order_by_extralink.$abc_extralink.$search_extralink.'">&gt;</a>';
			$pagerdiv .= ' | <a href="index.php?page=list&amp;page='.$pager['pages'].$order_by_extralink.$abc_extralink.$search_extralink.'">&gt;&gt;</a>';
		}
		$limit = " LIMIT ".$pager['first'].", ".$pager['items'];
	} else {
		$limit = '';
	}

	echo '<div class="grid-container';
	if(!isset($_REQUEST['search']) and !isset($_REQUEST['abc'])) {
		echo ' hidden"';
	}
	echo '" id="search-container"';
	echo '>';
	echo '<fieldset class="fieldset">';
	echo '<legend>'.lng('advanced_search').'</legend>';

	echo '<div class="grid-x grid-margin-x">';
	echo '<div class="cell small-12">';
	//echo '<br />';

	////////////////
	// Search box //
	////////////////


	echo '<form action="index.php?view=list" method="POST">';
	//echo '<input type="hidden" name="search" value="true" />';
	if(isset($_REQUEST['abc']) and $_REQUEST['abc'] != '') {
		echo '<input type="hidden" name="abc" value="'.$_REQUEST['abc'].'" />';
	}
	if(isset($_REQUEST['order']) and $_REQUEST['order'] != '') {
		echo '<input type="hidden" name="order" value="'.$_REQUEST['order'].'" />';
	}
	echo '<div class="grid-container">';
	echo '<div class="grid-x grid-padding-x">';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('title');
	echo '<input name="title" type="text" value="'.$_REQUEST['title'].'">';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('title_original');
	echo '<input name="title_original" type="text" value="'.$_REQUEST['title_original'].'">';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('author');
	echo '<input name="author" type="text" value="'.$_REQUEST['author'].'">';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('language');
	echo '<select name="language">';
	echo '<option value=""></option>';
	$result_languages = mysqli_query($link, "SELECT DISTINCT books_language FROM books ORDER BY books_language");
	while($myrow_languages = mysqli_fetch_assoc($result_languages)) {
		if($myrow_languages['books_language'] != '') {
			echo '<option value="'.$myrow_languages['books_language'].'"';
			if($_REQUEST['language'] == $myrow_languages['books_language']) {
				echo ' selected="selected"';
			}
			echo '>'.$myrow_languages['books_language'].'</option>';
		}
	}
	echo '<option value="N/A"';
	if($_REQUEST['language'] == 'N/A') {
		echo ' selected="selected"';
	}
	echo '>'.lng('na').'</option>';
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('world');
	echo '<input name="world" type="text" value="'.$_REQUEST['world'].'">';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('series');
	echo '<input name="series" type="text" value="'.$_REQUEST['series'].'">';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('genre');
	echo '<select name="genre">';
	echo '<option value=""></option>';
	$result_genres = mysqli_query($link, "SELECT DISTINCT books_genre FROM books ORDER BY books_genre");
	while($myrow_genres = mysqli_fetch_assoc($result_genres)) {
		if($myrow_genres['books_genre'] != '') {
			echo '<option value="'.$myrow_genres['books_genre'].'"';
			if($_REQUEST['genre'] == $myrow_genres['books_genre']) {
				echo ' selected="selected"';
			}
			echo '>'.$myrow_genres['books_genre'].'</option>';
		}
	}
	echo '<option value="N/A"';
	if($_REQUEST['genre'] == 'N/A') {
		echo ' selected="selected"';
	}
	echo '>'.lng('na').'</option>';
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('publisher');
	echo '<select name="publisher">';
	echo '<option value=""></option>';
	$result_publishers = mysqli_query($link, "SELECT DISTINCT books_publisher FROM books ORDER BY books_publisher");
	while($myrow_publishers = mysqli_fetch_assoc($result_publishers)) {
		if($myrow_publishers['books_publisher'] != '') {
			echo '<option value="'.$myrow_publishers['books_publisher'].'"';
			if($_REQUEST['publisher'] == $myrow_publishers['books_publisher']) {
				echo ' selected="selected"';
			}
			echo '>'.$myrow_publishers['books_publisher'].'</option>';
		}
	}
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('year_of_publication');
	echo '<input name="year" type="text" value="'.$_REQUEST['year'].'">';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('year_of_first_publication');
	echo '<input name="year_first" type="text" value="'.$_REQUEST['year_first'].'">';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('on_my_reading_list');
	echo '<select name="reading_list">';
	echo '<option value=""></option>';
	echo '<option value="0"';
	if($_REQUEST['reading_list'] == '0') {
		echo ' selected="selected"';
	}
	echo '>'.lng('no').'</option>';
	echo '<option value="1"';
	if($_REQUEST['reading_list'] == '1') {
		echo ' selected="selected"';
	}
	echo '>'.lng('yes').'</option>';
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('on_my_wishlist');
	echo '<select name="wishlist">';
	echo '<option value=""></option>';
	echo '<option value="0"';
	if($_REQUEST['wishlist'] == '0') {
		echo ' selected="selected"';
	}
	echo '>'.lng('no').'</option>';
	echo '<option value="1"';
	if($_REQUEST['wishlist'] == '1') {
		echo ' selected="selected"';
	}
	echo '>'.lng('yes').'</option>';
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('already_read');
	echo '<select name="already_read">';
	echo '<option value=""></option>';
	echo '<option value="0"';
	if($_REQUEST['already_read'] == '0') {
		echo ' selected="selected"';
	}
	echo '>'.lng('no').'</option>';
	echo '<option value="1"';
	if($_REQUEST['already_read'] == '1') {
		echo ' selected="selected"';
	}
	echo '>'.lng('yes').'</option>';
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('physical_copy');
	echo '<select name="physical_copy">';
	echo '<option value=""></option>';
	echo '<option value="0"';
	if($_REQUEST['physical_copy'] == '0') {
		echo ' selected="selected"';
	}
	echo '>'.lng('no').'</option>';
	echo '<option value="1"';
	if($_REQUEST['physical_copy'] == '1') {
		echo ' selected="selected"';
	}
	echo '>'.lng('yes').'</option>';
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('missing_book');
	echo '<select name="missing">';
	echo '<option value=""></option>';
	echo '<option value="0"';
	if($_REQUEST['missing'] == '0') {
		echo ' selected="selected"';
	}
	echo '>'.lng('no').'</option>';
	echo '<option value="1"';
	if($_REQUEST['missing'] == '1') {
		echo ' selected="selected"';
	}
	echo '>'.lng('yes').'</option>';
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-6 large-6 cell">';
	echo '<label>'.lng('missing_cover');
	echo '<select name="missing_cover">';
	echo '<option value=""></option>';
	echo '<option value="0"';
	if($_REQUEST['missing_cover'] == '0') {
		echo ' selected="selected"';
	}
	echo '>'.lng('no').'</option>';
	echo '<option value="1"';
	if($_REQUEST['missing_cover'] == '1') {
		echo ' selected="selected"';
	}
	echo '>'.lng('yes').'</option>';
	echo '</select>';
	echo '</label>';
	echo '</div>';
	////////////////////////
	echo '<div class="small-12 medium-4 large-4 medium-offset-4 large-offset-4 cell text-center">';
	echo '<br />';
	echo '<input type="submit" class="button expanded" value="'.lng('search').'" />';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
	//echo '<div class="cell small-12 text-center padding-top">';
	echo '<div class="grid-x grid-margin-x">';
	echo '<div class="cell small-12 text-center">';
	echo '<br />';

	


	if(!isset($_REQUEST['abc'])) {
		$_REQUEST['abc'] = '';
	}
	$abc_array = array('0-9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',  'y',  'z');
	$first = true;
	foreach($abc_array as $value) {
		if($first == true) {
			$first = false;
		} else {
			echo ' | ';
		}
		if($_REQUEST['abc'] == $value) {
			echo '<b>'.strtoupper($value).'</b>';
		} else {
			echo '<a href="index.php?view=list&amp;abc='.$value.$order_by_extralink.$search_extralink.'">';
			echo strtoupper($value);
			echo '</a>';
		}
	}

	//echo '<br />';

	echo '</div>';
	echo '</div>';
	echo '</fieldset>';

	
	echo '</div>';


	//active_filters
	$active_search = '';
	if(isset($_REQUEST['titleauthor']) and $_REQUEST['titleauthor'] != '') {
		if($active_search != '') {$active_search .= ' | ';}
		$active_search .= '<a href="index.php" title="'.lng('clear_search').'"><i class="fi-x red"></i></a> ';
		$active_search .= '<b>'.lng('title_or_author').':</b> '.$_REQUEST['titleauthor'];
	}

	foreach($search_array as $search_key) {
		if(isset($_REQUEST[$search_key]) and $_REQUEST[$search_key] != '') {
			if($active_search != '') {$active_search .= ' | ';}
			$active_search .= '<a href="index.php?clear_search_'.$search_key.'" title="'.lng('clear_search').'"><i class="fi-x red"></i></a> ';
			$active_search .= '<b>'.$search_array_lng[$search_key].':</b> '.$_REQUEST[$search_key];
		}
	}

	foreach($search_array_binary as $search_key) {
		if(isset($_REQUEST[$search_key]) and $_REQUEST[$search_key] != '') {
			if($active_search != '') {$active_search .= ' | ';}
			$active_search .= '<a href="index.php?clear_search_'.$search_key.'" title="'.lng('clear_search').'"><i class="fi-x red"></i></a> ';
			$active_search .= '<b>'.$search_array_binary_lng[$search_key].':</b> ';
			if($_REQUEST[$search_key] == 1) {$active_search .= lng('yes');}
			if($_REQUEST[$search_key] == 0) {$active_search .= lng('no');}
		}
	}

	if($active_search != '') {
		echo '<div class="grid-container">';
		echo '<fieldset class="fieldset">';
		echo '<legend>'.lng('active_search').'</legend>';
		echo $active_search;
		echo '</fieldset>';
		echo '</div>';
	}

?>

<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12 medium-12 padding-top small-text-center text-right">
			<a id="search-button"><i class="fi-magnifying-glass"></i> <span class="hide-for-small-only"><?php echo lng('advanced_search'); ?></span><span class="show-for-small-only"><?php echo lng('search'); ?></span></a>
			&nbsp;|&nbsp;
			<a href="index.php?clear_search"><i class="fi-x"></i> <?php echo lng('clear_search'); ?></a>
			<br />
			<br />
		</div>
	</div>
</div>

<?php
	if($pager['total'] == 0) {
		echo '<br />';
		echo '<div class="grid-container">';
		echo '<div class="callout warning">';
		echo '<h5>'.lng('your_search_yielded_no_results').'</h5>';
		echo '<p>'.lng('clear_or_modify_your_search_or_add_new_items').'</p>';
		echo '</div>';
		echo '</div>';
		return;
	}	
?>

<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12 medium-6 padding-top small-text-center">
			<?php
				echo lng('total').': '.$pager['total'];
				echo '&nbsp;&nbsp;&nbsp;';
				echo $pagerdiv;
			?>
			<br />
			<br />
		</div>
		<div class="cell small-4 medium-3 text-right small-text-center padding-top">
			<?php echo lng('order_by').':&nbsp;'; ?>
		</div>
		<div class="cell small-8 medium-3 small-text-center">
			<?php
				echo '<select id="order">';
				echo '<option value="index.php?view=list&amp;order=id'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == '' or $_REQUEST['order'] == 'id') {
					echo ' selected="selected"';
				}
				echo '>'.lng('new').'</option>';
				echo '<option value="index.php?view=list&amp;order=title'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == 'title') {
					echo ' selected="selected"';
				}
				echo '>'.lng('title').'</option>';
				echo '<option value="index.php?view=list&amp;order=title_original'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == 'title_original') {
					echo ' selected="selected"';
				}
				echo '>'.lng('title_original').'</option>';
				echo '<option value="index.php?view=list&amp;order=author'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == 'author') {
					echo ' selected="selected"';
				}
				echo '>'.lng('author').'</option>';
				echo '<option value="index.php?view=list&amp;order=genre'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == 'genre') {
					echo ' selected="selected"';
				}
				echo '>'.lng('genre').'</option>';
				echo '<option value="index.php?view=list&amp;order=world'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == 'world') {
					echo ' selected="selected"';
				}
				echo '>'.lng('world').'</option>';
				echo '<option value="index.php?view=list&amp;order=series'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == 'series') {
					echo ' selected="selected"';
				}
				echo '>'.lng('series').'</option>';
				echo '<option value="index.php?view=list&amp;order=year'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == 'year') {
					echo ' selected="selected"';
				}
				echo '>'.lng('year_of_publication').'</option>';
				echo '<option value="index.php?view=list&amp;order=year_first'.$abc_extralink.$search_extralink.'"';
				if($_REQUEST['order'] == 'year_first') {
					echo ' selected="selected"';
				}
				echo '>'.lng('year_of_first_publication').'</option>';
				echo '</select>';
			?>
			<br />
			<br />
		</div>
	</div>
</div>

<div class="grid-container">
	<div class="grid-x grid-margin-x">

		<?php
			$result = mysqli_query($link, "SELECT * FROM books WHERE $query $extraquery ORDER BY $order_by $limit");
			echo mysqli_error($link);

			while($myrow = mysqli_fetch_assoc($result)) {
				echo '<div class="cell small-6 medium-4 large-3 text-center list-item" onclick="location.href=\'index.php?view=details&amp;id='.$myrow['books_id'].'\';">';
				$image = getFilename('data/covers', $myrow['books_id'].'-13-');
				if($image == '') {
					$image = createImage('img/no-image.png', $list_image_max_width, $list_image_max_height);
				}
				echo '<a href="index.php?view=details&amp;id='.$myrow['books_id'].'">';
				echo '<img src="'.createImage($image, $list_image_max_width, $list_image_max_height).'" alt="" class="';
				if($_REQUEST['order'] != 'id' and $_REQUEST['order'] != 'title' and $_REQUEST['order'] != 'author') {
					echo 'list-cover-sort';
				} else {
					echo 'list-cover';
				}
				if($myrow['books_missing'] == 1) {
					echo ' list-cover-missing';
				} else {
					echo ' list-cover-not-missing';
				}
				echo '" ';
				if($myrow['books_missing'] == 1) {
					echo ' title="'.lng('missing_book').'"';
				}
				echo '/>';
				echo '</a>';
				echo '<div class="';
				if($_REQUEST['order'] != 'id' and $_REQUEST['order'] != 'title' and $_REQUEST['order'] != 'author') {
					echo 'list-data-sort';
				} else {
					echo 'list-data';
				}
				echo '">';
				echo '<span class="list-data-small-text">'.$myrow['books_author'].'</span>';
				echo '<br />';
				echo '<span class="list-data-normal-text">'.$myrow['books_title'].'</span>';
				if($_REQUEST['order'] == 'title_original') {
					echo '<br />';
					echo '<span class="list-data-small-text">&raquo; '.$myrow['books_title_original'].' &laquo;</span>';
				}
				if($_REQUEST['order'] == 'genre') {
					echo '<br />';
					echo '<span class="list-data-small-text">&raquo; '.$myrow['books_genre'].' &laquo;</span>';
				}
				if($_REQUEST['order'] == 'world') {
					echo '<br />';
					echo '<span class="list-data-small-text">&raquo; '.$myrow['books_world'].' &laquo;</span>';
				}
				if($_REQUEST['order'] == 'series') {
					echo '<br />';
					echo '<span class="list-data-small-text">&raquo; '.$myrow['books_series'].' &laquo;</span>';
				}
				if($_REQUEST['order'] == 'year') {
					echo '<br />';
					echo '<span class="list-data-small-text">&raquo; '.$myrow['books_year'].' &laquo;</span>';
				}
				if($_REQUEST['order'] == 'year_first') {
					echo '<br />';
					echo '<span class="list-data-small-text">&raquo; '.$myrow['books_year_first'].' &laquo;</span>';
				}
				echo '</div>';
				echo '</div>';
			}
		?>
	</div>
</div>

<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12 text-center">
			<?php
				$pagerdiv = str_replace(' id="previous_page_link"', '', $pagerdiv);
				$pagerdiv = str_replace(' id="next_page_link"', '', $pagerdiv);
				echo $pagerdiv;
			?>
			<br />
			<br />
		</div>
	</div>
</div>