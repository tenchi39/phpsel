<?php

	$result = mysqli_query($link, "SELECT * FROM books WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['id'])."' LIMIT 1");
	$myrow = mysqli_fetch_assoc($result);

	if(isset($_REQUEST['email_to_device']) and $_SESSION['guest_session'] == false) {

		/*require('vendor/phpmailer/Exception.php');
		require('vendor/phpmailer/PHPMailer.php');
		require('vendor/phpmailer/SMTP.php');
*/
		$mail = new PHPMailer\PHPMailer\PHPMailer();

	    //Server settings
	    $mail->SMTPDebug = 0;									// Enable verbose debug output
	    $mail->isSMTP();										// Set mailer to use SMTP
	    $mail->Host = $email_to_device_smtp_server;				// Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;									// Enable SMTP authentication
	    $mail->Username = $email_to_device_smtp_username;		// SMTP username
	    $mail->Password = $email_to_device_smtp_password;		// SMTP password
	    $mail->SMTPSecure = $email_to_device_smtp_security;		// Enable TLS encryption, `ssl` also accepted
	    $mail->Port = $email_to_device_smtp_port;				// TCP port to connect to

	    //Recipients
	    $mail->setFrom($email_to_device_from, 'phpSEL');
	    $mail->addAddress($email_to_device_address);     // Add a recipient

	    //Attachments
	    $mail->addAttachment($_REQUEST['email_to_device']);         // Add attachments

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Email to device';
	    $mail->Body    = $myrow['books_title'];
	    $mail->AltBody = $myrow['books_title'];

	    $mail->send();
		echo '<br />';
	    echo '<div class="grid-container">';
	    if($mail->ErrorInfo != '') {
			echo '<div class="callout alert">';
			echo '<h5>'.lng('error').'</h5>';
			echo '<p>'.$mail->ErrorInfo.'</p>';
			echo '</div>';
		} else {
			echo '<div class="callout success">';
			echo '<h5>'.lng('success').'</h5>';
			echo '<p>'.lng('your_book_has_been_sent_to_your_device').'</p>';
			echo '</div>';
		}
		echo '</div>';
	}

?>
<div class="grid-container">
	<div class="grid-x grid-margin-x">
		<div class="cell small-12 medium-5 large-3 text-center">
			<?php

				$files = array();
				for($i = 14;$i <= 17;$i++) {
					$filename = getFilename('data/books', $myrow['books_id'].'-'.$i.'-');
					if($filename != '') {
						$files[] = $filename;
					}
				}

				$image = getFilename('data/covers', $myrow['books_id'].'-13-');
				if($image == '') {
					$image = createImage('img/no-image.png', $details_image_max_width, $details_image_max_height);
					if($myrow['books_missing_cover'] == 0) {
						mysqli_query($link, "UPDATE books SET books_missing_cover=1 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['id'])."' LIMIT 1");
					}
				} else {
					if($myrow['books_missing_cover'] == 1) {
						mysqli_query($link, "UPDATE books SET books_missing_cover=0 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['id'])."' LIMIT 1");
					}
				}
				echo '<br />';
				echo '<img src="'.createImage($image, $details_image_max_width, $details_image_max_height).'" alt="" class="';
				if(empty($files)) {
					echo ' list-cover-missing';
				} else {
					echo ' list-cover-not-missing';
				}
				echo '" ';
				if(empty($files)) {
					echo ' title="'.lng('missing_book').'" ';
				}
				echo '/>';
				echo '<br />';

				// [Book controls]
				if($_SESSION['guest_session'] == false) {
					echo '<br />';
					echo '<a href="index.php?view=form&amp;id='.$myrow['books_id'].'">'.lng('edit_book').'</a>';
					echo ' | ';
					echo '<a href="index.php?view=list&delete_id='.$myrow['books_id'].'" onclick="return confirm(\''.lng('are_you_sure').'\')">'.lng('delete_book').'</a>';
				}
				if(is_array($search_links)) {
					foreach($search_links as $value) {
						echo '<br />';
						if($value[1] == 'author_title') {
							$search_term = $myrow['books_author'].' '.$myrow['books_title'];
						}
						if($value[1] == 'title') {
							$search_term = $myrow['books_title'];
						}
						echo '<a href="'.$value[0].urlencode($search_term).'" target="_blank">'.$value[2].'</a>';
					}
				}

				// if(class_exists('ZipArchive')) {
				// 	foreach($files as $filename) {
				// 		if(getFileExtension($filename) == 'cbz' or getFileExtension($filename) == 'cbr') {
				// 			echo '<br>';
				// 			echo '<a href="index.php?view=read&amp;id='.$_REQUEST['id'].'" data-open="readModal"><i class="fi-projection-screen"></i> '.lng('read_comic').'</a>';
				// 		}
				// 	}
				// }
				// [/Book controls]


			?>
		</div>
		<div class="cell small-12 medium-7 large-9">
			<br />
			<?php
				//////////
				echo '<h1 class="small-text-center">'.$myrow['books_title'].'</h1>';
				//////////





				// [Book data]
				echo '<fieldset class="fieldset">';
				echo '<legend>'.lng('information').'</legend>';
				if($myrow['books_title_original'] != '') {
					echo $lng['title_original'].': '.$myrow['books_title_original'].'<br />';

					$result_original = mysqli_query($link, "SELECT books_id, books_title, books_author FROM books WHERE books_title='".mysqli_real_escape_string($link, $myrow['books_title_original'])."' AND books_language!='".mysqli_real_escape_string($link, $myrow['books_language'])."'");
					while($myrow_original = mysqli_fetch_assoc($result_original)) {
						echo '<small>&gt;&gt; ';
						echo '<a href="index.php?view=list&amp;clear_search&amp;author='.urlencode($myrow_original['books_author']).'">'.$myrow_original['books_author'].'</a>: <a href="index.php?view=details&id='.$myrow_original['books_id'].'">'.$myrow_original['books_title'].'</a></small></br />';
					}
				}
				//////////
				if($myrow['books_author'] != '') {
					echo lng('author').': ';
					$authors = array();
					$element = explode(',', $myrow['books_author']);
					foreach($element as $value) {
						$value = trim($value);
						if($value != '' and !in_array($value, $authors)) {
							$authors[] = $value;
						}
					}
					if(!empty($authors)) {
						sort($authors);
						reset($authors);
						$first = true;
						foreach($authors as $value) {
							if($first == true) {
								$first = false;
							} else {
								echo ', ';
							}
							echo '<a href="index.php?view=list&amp;clear_search&amp;author='.urlencode($value).'">'.$value.'</a>';
						}
					}
					echo '<br />';
				}
				//////////
				if($myrow['books_genre'] != '') {
					echo lng('genre').': ';
					$genres = array();
					$element = explode(',', $myrow['books_genre']);
					foreach($element as $value) {
						$value = trim($value);
						if($value != '' and !in_array($value, $genres)) {
							$genres[] = $value;
						}
					}
					if(!empty($genres)) {
						sort($genres);
						reset($genres);
						$first = true;
						foreach($genres as $value) {
							if($first == true) {
								$first = false;
							} else {
								echo ', ';
							}
							echo '<a href="index.php?view=list&amp;clear_search&amp;genre='.urlencode($value).'">'.$value.'</a>';
						}
					}
					echo '<br />';
				}
				//////////
				if($myrow['books_world'] != '') {
					echo lng('world').': <a href="index.php?view=list&amp;clear_search&amp;world='.urlencode($myrow['books_world']).'">'.$myrow['books_world'].'</a>';
					if($myrow['books_world_number'] != 0) {
						echo ' #'.$myrow['books_world_number'];
					}
					echo '<br />';
				}
				//////////
				if($myrow['books_series'] != '') {
					echo lng('series').': <a href="index.php?view=list&amp;clear_search&amp;series='.urlencode($myrow['books_series']).'">'.$myrow['books_series'].'</a>';
					if($myrow['books_series_number'] != 0) {
						echo ' #'.$myrow['books_series_number'];
					}
					echo '<br />';
				}
				//////////
				if($myrow['books_publisher'] != '') {
					echo lng('publisher').': <a href="index.php?view=list&amp;clear_search&amp;publisher='.urlencode($myrow['books_publisher']).'">'.$myrow['books_publisher'].'</a><br />';
				}
				//////////
				if($myrow['books_year'] != '') {
					echo lng('year_of_publication').': <a href="index.php?view=list&amp;clear_search&amp;year='.urlencode($myrow['books_year']).'">'.$myrow['books_year'].'</a><br />';
				}
				//////////
				if($myrow['books_year_first'] != '') {
					echo lng('year_of_first_publication').': <a href="index.php?view=list&amp;clear_search&amp;year_first='.urlencode($myrow['books_year_first']).'">'.$myrow['books_year_first'].'</a><br />';
				}
				//////////
				if($myrow['books_language'] != '') {
					echo lng('language').': <a href="index.php?view=list&amp;clear_search&amp;language='.urlencode($myrow['books_language']).'">'.$myrow['books_language'].'</a><br />';
				}
				echo lng('added_to_collection').': '.humanReadableDateAndTime($myrow['books_time'], $time_format).'<br />';
				
				echo '<br />';
				if($_SESSION['guest_session'] == false) {
					// [Book status]
					///////////////////////////////////
					echo '<span id="ajax_set_read">';
					echo '<input type="checkbox" id="already_read" name="already_read"';
					if($myrow['books_already_read'] == 1) {
					 echo ' checked="checked"';
					}
					echo ' onclick="ajaxContent(\'lib/ajax_set_read.php?books_id='.$myrow['books_id'].'\', \'ajax_set_read\')">';
					echo '<label for="already_read">'.lng('already_read').'</label>';
					echo '</span>';
					echo '<br />';
					///////////////////////////////////
					echo '<span id="ajax_set_reading_list">';
					echo '<input type="checkbox" id="reading_list" name="reading_list"';
					if($myrow['books_reading_list'] == 1) {
					 echo ' checked="checked"';
					}
					echo ' onclick="ajaxContent(\'lib/ajax_set_reading_list.php?books_id='.$myrow['books_id'].'\', \'ajax_set_reading_list\')">';
					echo '<label for="reading_list">'.lng('reading_list').'</label>';
					echo '</span>';
					echo '<br />';
					///////////////////////////////////
					echo '<span id="ajax_set_wishlist">';
					echo '<input type="checkbox" id="wishlist" name="wishlist"';
					if($myrow['books_wishlist'] == 1) {
					 echo ' checked="checked"';
					}
					echo ' onclick="ajaxContent(\'lib/ajax_set_wishlist.php?books_id='.$myrow['books_id'].'\', \'ajax_set_wishlist\')">';
					echo '<label for="wishlist">'.lng('wishlist').'</label>';
					echo '</span>';
					echo '<br />';
					///////////////////////////////////
					echo '<span id="ajax_set_physical_copy">';
					echo '<input type="checkbox" id="physical_copy" name="physical_copy"';
					if($myrow['books_physical_copy'] == 1) {
					 echo ' checked="checked"';
					}
					echo ' onclick="ajaxContent(\'lib/ajax_set_physical_copy.php?books_id='.$myrow['books_id'].'\', \'ajax_set_physical_copy\')">';
					echo '<label for="physical_copy">'.lng('physical_copy').'</label>';
					echo '</span>';
					// echo '<br />';
					///////////////////////////////////
					// [/Book status]
				}


				echo '</fieldset>';
				// [/Book data]





				// [Book synopsis]
				if($myrow['books_synopsis'] != '') {
					// echo '<br />';
					// echo '<br />';
					echo '<fieldset class="fieldset">';
    				echo '<legend>'.lng('synopsis').'</legend>';
					echo nl2br($myrow['books_synopsis']);
					echo '<br />';
					echo '<br />';
					echo '</fieldset>';
				}
				// [/Book synopsis]
				// echo '<br />';

				// [Files]
				if(!empty($files)) {
					mysqli_query($link, "UPDATE books SET books_missing=0 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['id'])."' LIMIT 1");
					echo '<fieldset class="fieldset">';
    				echo '<legend>'.lng('files').'</legend>';
					foreach($files as $value) {
						$filename = explode('/', $value);
						$filename = $filename[count($filename)-1];
						$extension = explode('.', $value);
						$extension = $extension[count($extension)-1];
						echo $filename;
						echo ' ['.humanReadableSize(filesize($value)).']';
						echo '<br />';
						echo '<a href="data/books/'.rawurlencode($filename).'" target="_blank"><i class="fi-download"></i> '.lng('download').' .'.strtolower($extension).'</a>&nbsp;';
						if($qr_code == true) {
							echo '<br />';
							echo '<a href="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='.$baseurl.'/'.$value.'" target="_blank"><i class="fi-camera"></i> ';
							echo lng('qr_code').'</a>';
						}
						if($email_to_device == true and $_SESSION['guest_session'] == false) {
							echo '<br />';
							if(!in_array(strtoupper($extension), $email_to_device_formats)) {
								echo '<a class="opacity02"><i class="fi-mail"></i> '.lng('email_to_device').'</a>&nbsp;';
							} else {
								echo '<a href="index.php?view=details&amp;id='.$_REQUEST['id'].'&amp;email_to_device='.$value.'"><i class="fi-mail"></i> '.lng('email_to_device').'</a>&nbsp;';
							}
						}
						if(class_exists('ZipArchive')) {
							if(getFileExtension($value) == 'cbz' or getFileExtension($value) == 'cbr') {
								echo '<br />';
								echo '<a href="index.php?view=read&amp;id='.$_REQUEST['id'].'" data-open="readModal"><i class="fi-projection-screen"></i> '.lng('read_comic').'</a>';
							}
						}
						echo '<br />';
						echo '<br />';
					}
					echo '</fieldset>';
				} else {
					mysqli_query($link, "UPDATE books SET books_missing=1 WHERE books_id='".mysqli_real_escape_string($link, $_REQUEST['id'])."' LIMIT 1");
				}
				// [/Files]
				if($myrow['books_series'] != '' or $myrow['books_world'] != '' or $myrow['books_author'] != '') {
					echo '<fieldset class="fieldset">';
    				echo '<legend>'.lng('related_books').'</legend>';


					//Sorozat
					if($myrow['books_series'] != '') {
						echo '<h5>'.lng('series').': <a href="index.php?view=list&amp;clear_search&amp;series='.urlencode($myrow['books_series']).'">'.$myrow['books_series'].'</a></h5>';
						$result2 = mysqli_query($link, "SELECT * FROM books WHERE books_series='".mysqli_real_escape_string($link, $myrow['books_series'])."' AND books_world='".mysqli_real_escape_string($link, $myrow['books_world'])."' AND books_language='".mysqli_real_escape_string($link, $myrow['books_language'])."' ORDER BY books_series, books_series_number, books_title");
						while($myrow2 = mysqli_fetch_assoc($result2)) {
							if($myrow2['books_series_number'] != '' and $myrow2['books_series_number'] != 0) {
								echo '#'.$myrow2['books_series_number'].' ';
							}
							if($myrow2['books_title'] == $myrow['books_title']) {
								echo '<b>'.$myrow2['books_title'].'</b>';
							} else {
								echo '<a href="index.php?view=details&amp;id='.$myrow2['books_id'].'">'.$myrow2['books_title'].'</a>';
							}
							echo '<br />';
						}
						echo '<br />';
					}
					//Világ
					if($myrow['books_world'] != '') {
						echo '<h5>'.lng('world').': <a href="index.php?view=list&amp;clear_search&amp;world='.urlencode($myrow['books_world']).'">'.$myrow['books_world'].'</a></h5>';
						$result2 = mysqli_query($link, "SELECT * FROM books WHERE books_world='".mysqli_real_escape_string($link, $myrow['books_world'])."' AND books_language='".mysqli_real_escape_string($link, $myrow['books_language'])."' ORDER BY books_world, books_world_number, books_series, books_series_number, books_title");
						while($myrow2 = mysqli_fetch_assoc($result2)) {
							if($myrow2['books_world_number'] != '' and $myrow2['books_world_number'] != 0) {
								echo '#'.$myrow2['books_world_number'].' ';
							} else {
								echo '- ';
							}
							if($myrow2['books_title'] == $myrow['books_title']) {
								echo '<b>'.$myrow2['books_title'].'</b>';
							} else {
								echo '<a href="index.php?view=details&amp;id='.$myrow2['books_id'].'">'.$myrow2['books_title'].'</a>';
							}
							echo '<br />';
						}
						echo '<br />';
					}
					//Szerző
					if($myrow['books_author'] != '') {
						echo '<h5>'.lng('author').': <a href="index.php?view=list&amp;clear_search&amp;author='.urlencode($myrow['books_author']).'">'.$myrow['books_author'].'</a></h5>';
						$result2 = mysqli_query($link, "SELECT * FROM books WHERE books_author='".mysqli_real_escape_string($link, $myrow['books_author'])."' AND books_language='".mysqli_real_escape_string($link, $myrow['books_language'])."' ORDER BY books_title");
						while($myrow2 = mysqli_fetch_assoc($result2)) {
							echo '- ';
							if($myrow2['books_title'] == $myrow['books_title']) {
								echo '<b>'.$myrow2['books_title'].'</b>';
							} else {
								echo '<a href="index.php?view=details&amp;id='.$myrow2['books_id'].'">'.$myrow2['books_title'].'</a>';
							}
							echo '<br />';
						}
						echo '<br />';
					}
					echo '</fieldset>';
				}



			?>
		</div>
	</div>
</div>

<div class="reveal" id="readModal" data-reveal>
  <h3><?php echo lng('loading_comics_reader'); ?> <img src="img/spinner.gif" alt=""></h3>
  <p class="lead"><?php echo lng('extracting_archive_to_temporary_directory'); ?></p>
  <p><?php echo lng('please_be_patient_this_might_take_a_while'); ?></p>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
