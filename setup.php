<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	header('Content-type: text/html; charset=utf-8'); 
	session_name('phpsel-setup');
	session_start();

	// configuration
	if(is_file('config.php')) {
		header('Location: index.php');
		exit();
	}

	// functions
	include('lib/functions.php');
	// language
	include('lng/en.php');

?><!doctype html>
<html class="no-js" lang="en">
<head>
	<!-- meta -->
	<meta charset="utf-8" />
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>phpSEL Setup</title>
	<link rel="stylesheet" href="vendor/zurb/foundation/dist/css/foundation.css">
	<link rel="stylesheet" href="css/foundation-icons.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body class="install-body">

	<div id="main">

		<br />
		<br />
		<br />
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-10 small-offset-1 medium-6 large-6 medium-offset-3 large-offset-3">

					<div class="callout">
						<?php 
							if(!isset($_REQUEST['step']) or $_REQUEST['step'] == '') {
								$_REQUEST['step'] = 0;
							};
							include('lib/setup_step_'.$_REQUEST['step'].'.php');
						?>
					</div>
				</div>
			</div>
		</div>
	</div>




	<script src="vendor/components/jquery/jquery.min.js"></script>
	<script src="vendor/zurb/foundation/dist/js/foundation.min.js"></script>
	<script src="lib/functions.js"></script>

	<script>
		$(document).ready(function() {
			$(document).foundation();
		});
	</script>

</body>
</html>