#!/bin/bash
# Make a release from the freshly cloned git source code of phpSEL

# Install necessary files
php composer.phar install

# Remove unneeded files from Fancybox
mv ./vendor/lagman/fancybox/source ./vendor/lagman/source
rm -rf ./vendor/lagman/fancybox
mkdir ./vendor/lagman/fancybox
mv ./vendor/lagman/source ./vendor/lagman/fancybox/source

# Remove unneeded files from Foundation
mv ./vendor/zurb/foundation/dist ./vendor/zurb/dist
rm -rf ./vendor/zurb/foundation
mkdir ./vendor/zurb/foundation
mv ./vendor/zurb/dist ./vendor/zurb/foundation/dist

# Remove phpSEL git files
rm -rf ./.git
rm ./.gitignore

# Get current version
value=`cat version.txt`
echo "$value"

# Zip everything in current directory
zip -r phpsel-$value.zip .