<?php

	header('Content-type: text/html; charset=utf-8'); 
	// configuration
	include('config.php');

	// session
	session_name($db_database);
	session_start();

	// functions
	include('lib/functions.php');
	
	// language
	include('lng/'.$site_language.'.php');

	if(!isset($_REQUEST['username'])) {
		$_REQUEST['username'] = '';
	}

	if(!isset($_REQUEST['password'])) {
		$_REQUEST['password'] = '';
	}

	if(isset($_REQUEST['submitted'])) {
		if($_REQUEST['username'] == $login_username and $_REQUEST['password'] == $login_password) {
			$_SESSION['logged_in'] = true;
			$_SESSION['guest_session'] = false;
		}
		if($_REQUEST['username'] == $guest_username and $_REQUEST['password'] == $guest_password) {
			$_SESSION['logged_in'] = true;
			$_SESSION['guest_session'] = true;
		}
	}


	if(isset($_SESSION['logged_in'])) {
		header("Location: ".$baseurl."/index.php");
	}

?>


<!doctype html>
<html class="no-js" lang="<?php echo $site_language; ?>">
<head>
	<!-- meta -->
	<base href="<?php echo $baseurl; ?>/" />
	<meta charset="utf-8" />
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $site_title; ?></title>
	<link rel="stylesheet" href="vendor/zurb/foundation/dist/css/foundation.css">
	<link rel="stylesheet" href="css/foundation-icons.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>

	<div id="main">

		<br />
		<br />
		<br />
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-10 small-offset-1 medium-6 large-4 medium-offset-3 large-offset-4">
					<form class="log-in-form" action="<?php echo $baseurl; ?>/login.php" method="POST">
						<input type="hidden" name="submitted" value="true">
						<h4 class="text-center"><?php echo lng('log_in_to_phpsel'); ?></h4>
						<br />
						<?php
							if(isset($_REQUEST['submitted'])) {
								if($_REQUEST['username'] != $login_username or $_REQUEST['password'] != $login_password) {
									echo '<div class="callout alert">';
										echo '<h5>'.lng('error').'</h5>';
										echo '<p>'.lng('incorrect_username_or_password').'</p>';
									echo '</div>';
								}
							}
						?>
						<label><?php echo lng('username'); ?>
							<input type="text" name="username" value="<?php echo $_REQUEST['username']; ?>">
						</label>
						<label><?php echo lng('password'); ?>
							<input type="password" name="password" value="<?php echo $_REQUEST['password']; ?>">
						</label>
						<br />
						<p><input type="submit" class="button expanded" value="<?php echo lng('log_in'); ?>"></input></p>
					</form>
					<?php echo $login_message; ?>
				</div>
			</div>
		</div>
	</div>




	<script src="vendor/components/jquery/jquery.min.js"></script>
	<script src="vendor/zurb/foundation/dist/js/foundation.min.js"></script>
	<script src="lib/functions.js"></script>

	<script>
		$(document).ready(function() {
			$(document).foundation();
		});
	</script>

</body>
</html>