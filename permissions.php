<?php

	header('Content-type: text/html; charset=utf-8'); 


	// configuration
	include('config.php');
	// session
	session_name($db_database);
	session_start();
	// functions
	include('lib/functions.php');
	// language
	include('lng/'.$site_language.'.php');

	$permission_files = array('data/books', 'data/covers', 'data/tmp', 'data/extracted');

	$errors = false;
	foreach($permission_files as $file) {
		if(!is_writable($file)) {
			if($file == 'version_check.txt' or $file == 'version_remote.txt') {
				if(!is_file($file)) {
					$handle = @fopen($file, "w");
					@fclose($handle);
					if(!is_file($file)) {
						$errors = true;
					}
				} else {
					$errors = true;
				}
			} else {
				$errors = true;
			}
		}
	}
	if($errors == false) {
		header("Location: ".$baseurl."/");
		exit();
	}

?><!doctype html>
<html class="no-js" lang="<?php echo $site_language; ?>">
<head>
	<!-- meta -->
	<base href="<?php echo $baseurl; ?>/" />
	<meta charset="utf-8" />
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $site_title; ?></title>
	<link rel="stylesheet" href="vendor/zurb/foundation/dist/css/foundation.css">
	<link rel="stylesheet" href="css/foundation-icons.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>

	<div id="main">

		<br />
		<br />
		<br />
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<div class="cell small-10 small-offset-1 medium-6 large-6 medium-offset-4 large-offset-3">

					<div class="callout alert">
						<h5><?php echo lng('incorrect_permissions'); ?></h5>
						<p><?php echo lng('please_set_the_following_writable'); ?></p>
						<ul>
						<?php
							foreach($permission_files as $file) {
								if(!is_writable($file)) {
									if(!is_file($file)) {
										echo '<li>/'.$file.' - '.lng('unable_to_create_file').'</li>';
									} else {
										echo '<li>/'.$file.'</li>';
									}
								}
							}
						?>
						</ul>
						<br />
						<a class="button" href="#" onclick="location.reload();"><?php echo lng('try_again'); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>




	<script src="vendor/components/jquery/jquery.min.js"></script>
	<script src="vendor/zurb/foundation/dist/js/foundation.min.js"></script>
	<script src="lib/functions.js"></script>

	<script>
		$(document).ready(function() {
			$(document).foundation();
		});
	</script>

</body>
</html>